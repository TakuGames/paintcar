﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LocationPanel : MonoBehaviour
{
    [SerializeField] private RectTransform _locationPanel = null;
    [SerializeField] private TextMeshProUGUI _locationText;
    [SerializeField] private TextMeshProUGUI _locationStars;

    private List<RectTransform> _rects = new List<RectTransform>();
    private List<string> _levelsNames = new List<string>();

    private float _posY;

    private void OnEnable()
    {
        ChangesStars();
    }

    void Start()
    {
        var children = GetComponentsInChildren<RectTransform>();
        var levelData = Reference.instance.levelsData;

        for (int i = 0; i < levelData.locationsCount; i++)
        {
            _levelsNames.Add(levelData.GetLocation(i).name.ToUpper());
        }

        foreach (var child in children)
        {
            if (child.name.Contains("LocationGroup"))
            {
                _rects.Add(child);
            }
        }
    }

    void LateUpdate()
    {
        _posY = _locationPanel.transform.position.y;

        for (int i = 0; i < _levelsNames.Count; i++)
        {
            float worldPosYItem = _rects[i].TransformPoint(Vector2.one * (_rects[i].rect.height / 2)).y;

            if (Mathf.Abs(worldPosYItem - _posY) <= 2f)
            {
                _locationText.text = _levelsNames[i];
            }
        }
    }

    public void ChangesStars()
    {
        _locationStars.text = $"{Reference.instance.levelsData.GetStarsCount()}";
    }
}

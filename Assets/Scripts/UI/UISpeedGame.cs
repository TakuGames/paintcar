﻿using UnityEngine;
using UnityEngine.UI;

public class UISpeedGame : MonoBehaviour
{
    [SerializeField] private Slider _slider;

    public void ChangeGameSpeed(float value)
    {
        Time.timeScale = value;
        _slider.value = Time.timeScale;
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LoaderLevel : MonoBehaviour
{
    [SerializeField] private SOLevelsData _levelsData;
    
    [SerializeField] private Slider _slider;
    [SerializeField] private TextMeshProUGUI _loadingText;

    [SerializeField] private bool _loadAsynch = true;
    [SerializeField] private bool _inStart = true;

    private Coroutine _cor = null;
    private float _progress = 0;

    private void Start()
    {
        _levelsData.SelectLevel();
        
        if (_inStart)
            LoadLevel();
    }

    IEnumerator LoadAsynch(int sceneIndex)
    {
        AsyncOperation opAsync = SceneManager.LoadSceneAsync(sceneIndex);
        opAsync.allowSceneActivation = false;

        WaitForEndOfFrame frame = new WaitForEndOfFrame();

        while (_progress < 1)
        {
            _progress = Mathf.Clamp01(opAsync.progress / 0.9f);

            yield return frame;

            if (_loadingText)
                _loadingText.text = $"Loading... {Mathf.RoundToInt(_progress * 100)} %";

            _slider.value = _progress;
        }

        opAsync.allowSceneActivation = true;
        _cor = null;
    }

    public void LoadLevel()
    {
        if (_loadAsynch)
        {
            if (_cor == null)
                _cor = StartCoroutine(LoadAsynch(1));
        }
        else
        {
            SceneManager.LoadSceneAsync(1);
        }
    }
}

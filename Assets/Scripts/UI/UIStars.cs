﻿using UnityEngine;
using UnityEngine.UI;

public class UIStars : MonoBehaviour
{
    [SerializeField] private Image[] _stars;
    private ScoreData _scoreConfig;

    private void Awake()
    {
        _scoreConfig = Reference.instance.score.scoreData;
    }

    private void Start()
    {
        _scoreConfig.OnStars.AddListener(SetStars);
    }

    private void OnDestroy()
    {
        _scoreConfig.OnStars.RemoveListener(SetStars);
    }

    private void SetStars()
    {
        for (int i = 0; i < _stars.Length; i++)
        {
            _stars[i].gameObject.SetActive(i < _scoreConfig.stars);
        }
    }
}
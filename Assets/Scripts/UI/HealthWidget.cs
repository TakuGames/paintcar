﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthWidget : MonoBehaviour
{
    [SerializeField] private GameObject _lifePrefub;

    private List<GameObject> _lifeObjects = new List<GameObject>();
    private PlayerConfig _config;

    private void Awake()
    {
        _config = Reference.instance.player.config;
    }

    void Start()
    {
        HealthPlayer.OnHealthChange.AddListener(ChangeHealth);
        
        AddLife();
    }

    private void OnDestroy()
    {
        HealthPlayer.OnHealthChange.RemoveListener(ChangeHealth);
    }

    private void ChangeHealth(int health)
    {
        for (int i = 0; i < _config.health.startHealth; i++)
        {
            _lifeObjects[i].SetActive(i < health);
        }
    }

    private void AddLife()
    {
        for (int i = 0; i < _config.health.startHealth; i++)
            _lifeObjects.Add(Instantiate(_lifePrefub, transform));
    }
}

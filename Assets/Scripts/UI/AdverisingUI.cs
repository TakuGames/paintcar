﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class AdverisingUI : MonoBehaviour
{
    public static UnityEvent OnAds = new UnityEvent();

    [SerializeField] private GameObject _advertising;
    [SerializeField] private TextMeshProUGUI _number;
    [SerializeField] private int _countdown;
    private PlayerHealth _health;

    private bool closed = true;

    private void Start()
    {
        _health = GameObject.FindWithTag("Player").GetComponent<PlayerHealth>();
    }

    public void EnableAdvertising(bool enable)
    {
        OnAds.Invoke();
        _advertising.SetActive(enable);

    }

    public void AddLife(int addedLifes)
    {
        _health.ChangeHealth(addedLifes);
        EnableAdvertising(true);
        CloseWindow();
    }

    public void StartCountdown()
    {
        closed = false;
        if (_number)
            StartCoroutine(Countdown());
    }

    private IEnumerator Countdown()
    {
        var wait = new WaitForSeconds(1f);
        for (int i = _countdown; i >= 0; i--)
        {
            _number.text = i.ToString();
            yield return wait;
        }
        CloseWindow();
    }

    public void CloseWindow()
    {
        if (!closed)
            OnAds.Invoke();
        closed = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWindow : MonoBehaviour
{
    public void OnStartBtn()
    {
        StateGame.OnPlayGame.Invoke();
        Reference.instance.effectsController.UseEffectOnce(EFX.rain);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILevelStars : MonoBehaviour
{
    [SerializeField] private LevelStartBtn[] _buttons;

    private void Awake()
    {
        _buttons = GetComponentsInChildren<LevelStartBtn>();
    }
}

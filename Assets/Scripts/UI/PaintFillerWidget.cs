﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintFillerWidget : MonoBehaviour
{
    private PlayerConfig _config;
    private Slider _slider;
    private Image _sliderColor;

    private void Awake()
    {
        _config = Reference.instance.player.config;
    }

    void Start()
    {
        _slider = gameObject.GetComponent<Slider>();
        _sliderColor = _slider.fillRect.gameObject.GetComponent<Image>();
        _slider.maxValue = _config.paint.maxPaint;

        ChangeSliderPosition();
        
        _config.paint.OnPaintChange.AddListener(ChangeSliderPosition);
    }

    private void OnDestroy()
    {
        _config.paint.OnPaintChange.RemoveListener(ChangeSliderPosition);
    }

    private void ChangeSliderPosition()
    {
        _slider.value = _config.paint.curPaint;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckAdvertisingEvent : UnityEvent<bool> { }

public class AdvertisingController : MonoBehaviour
{
    private bool _ads;
    public static CheckAdvertisingEvent OnAdsDead = new CheckAdvertisingEvent();
    public static CheckAdvertisingEvent OnAdsAmbulance = new CheckAdvertisingEvent();

    bool _isFirstDead;
    bool _isFirstHelp;

    private void OnEnable()
    {
        HealthPlayer.OnDeath.AddListener(CallDeath);
        //AmbulanceContact.AmbulanceEvent += CallAmbulance;
    }

    private void OnDisable()
    {
        HealthPlayer.OnDeath.RemoveListener(CallDeath);
        //AmbulanceContact.AmbulanceEvent -= CallAmbulance;
    }

    void Start()
    {
        _ads = false;
        _isFirstDead = true;
        StartCoroutine(CheckInternetConnection());
    }

    IEnumerator CheckInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            _ads = false;
        }
        else
        {
            _ads = true;
        }
    }

    private void CallDeath()
    {
        OnAdsDead.Invoke(_ads && _isFirstDead);
        _isFirstDead = false;
    }
    private void CallAmbulance()
    {
        OnAdsAmbulance.Invoke(_ads && _isFirstHelp);
        _isFirstHelp = false;
    }
}

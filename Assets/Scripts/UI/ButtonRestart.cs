﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ButtonRestart : MonoBehaviour
{
    public static UnityEvent OnRestart = new UnityEvent();

    public void Restarting()
    {
        OnRestart.Invoke();
    }
}

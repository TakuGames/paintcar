﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Data/ScoreData", fileName = "NewScoreData")]
public class ScoreData : ScriptableObject
{
    [SerializeField] private int _stars = 0;
    public int stars
    {
        get => _stars;
        set
        {
            _stars = value;
            if (_stars > 0)
                OnStars.Invoke();
        }
    }

    [SerializeField] private float _score = 0;
    public float score
    {
        get => _score;
        set
        {
            _score = value;
            if (_score > 0)
                OnScore.Invoke();
        }
    }
    [SerializeField] private float _deadEnemies = 0;
    public float deadEnemies
    {
        get => _deadEnemies;
        set
        {
            _deadEnemies = value;
            if (_deadEnemies > 0)
                OnEnemyDead.Invoke();
        }
    }


    public UnityEvent OnStars = new UnityEvent();
    public UnityEvent OnScore = new UnityEvent();
    public UnityEvent OnEnemyDead = new UnityEvent();
}
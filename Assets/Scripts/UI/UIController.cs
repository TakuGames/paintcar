﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    public Animator animator => _animator;
    
    public static UnityEvent OnRestartBtnEvent = new UnityEvent();
    public static UnityEvent OnNextLevelBtnEvent = new UnityEvent();
    public static UnityEvent OnExitBtnEvent = new UnityEvent();
    public static UnityEvent OnChooseLevelBtnEvent = new UnityEvent();
    
    public void OnRestartBtn()
    {
        OnRestartBtnEvent.Invoke();
    }
    
    public void OnNextLevelBtn()
    {
        OnNextLevelBtnEvent.Invoke();
    }

    public void OnCloseBtn()
    {
        OnExitBtnEvent.Invoke();
    }

    public void OnChooseLevelBtn()
    {
        OnChooseLevelBtnEvent.Invoke();
    }
}

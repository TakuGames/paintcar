﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelHeader : MonoBehaviour
{
    private TextMeshProUGUI _tmp;
    private void Start()
    {
        _tmp = GetComponentInChildren<TextMeshProUGUI>();

        if (_tmp)
            _tmp.text += $" {Reference.instance.levelsData.loaded.level + 1}";
    }
}

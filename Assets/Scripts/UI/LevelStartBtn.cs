﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelStartBtn : MonoBehaviour
{
    public Image[] StarsImages { get; set; }
    public Button CurrentButton { get; set; }

    [SerializeField] private TextMeshProUGUI _numberTxt;
    [SerializeField] private Transform _starParent;

    private void Awake()
    {
        CurrentButton = GetComponent<Button>();
        StarsImages = _starParent.GetComponentsInChildren<Image>();
    }
}

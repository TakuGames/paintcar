﻿using UnityEngine;
using UnityEngine.Events;

public class OnCloseEvent : UnityEvent<bool> { }
public class UIPanelLevelBtn : MonoBehaviour
{
    public static OnCloseEvent OnClose = new OnCloseEvent();

    public void ClosePanel(bool close)
    {
        OnClose.Invoke(close);
    }
}

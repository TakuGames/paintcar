﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.Serialization;

public class UIScore : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreTxt;
    [SerializeField] private Slider _slider;

    public void ShowScorePaint(float value)
    {
        if (Mathf.RoundToInt(value) > 1) return;

        _scoreTxt.text = $"Painted: {Mathf.RoundToInt(value * 100)} %";
        _slider.value = value;

        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class AdvertisingHealthPanelController : AAdvertisingPanelController
{
    

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
    {
        if (!firstEnter)
        {
            //AmbulanceContact.AmbulanceEvent += OpenAdvertising;
        }
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    private void onDestroy()
    {
        //AmbulanceContact.AmbulanceEvent -= OpenAdvertising;
    }

}

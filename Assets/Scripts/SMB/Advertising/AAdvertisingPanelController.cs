﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AAdvertisingPanelController : StateMachineBehaviour
{
    private Animator _animator = null;
    private PlayerConfig _config;
    protected bool firstEnter;

    private void Awake()
    {
        _config = Reference.instance.player.config;
        firstEnter = false;
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!firstEnter)
        {
            firstEnter = true;
        }
        _animator = animator;
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    


    protected virtual void OpenAdvertising()
    {
        _animator.SetBool("Open", true);
        //TODO: stop game
    }
}

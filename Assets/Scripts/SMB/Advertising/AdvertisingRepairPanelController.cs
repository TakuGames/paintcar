﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class AdvertisingRepairPanelController : AAdvertisingPanelController
{
    private bool isFirstShow = true;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!firstEnter)
        {
            isFirstShow = true;
            HealthPlayer.OnDeath.AddListener(OpenAdvertising);
        }
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    protected override void OpenAdvertising()
    {
        if (isFirstShow)
        {
            isFirstShow = false;
            base.OpenAdvertising();
        }
    }

    protected virtual void onDestroy()
    {
        HealthPlayer.OnDeath.RemoveListener(OpenAdvertising);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBWindows : StateMachineBehaviour
{
    
    public string START = "Start";
    public string END = "End";
    public string LOST = "Lost";
    public string STARS = "Stars";
    public string RESTART = "Restart";
    public string NEXTLEVEL = "NextLevel";
    public string EXIT = "Exit";
    public string CHOOSELEVEL = "ChooseLevel";

    private Animator _animator;
    private ScoreData _scoreData;

    private void Awake()
    {
        _animator = Reference.instance.ui.animator;
        
        _scoreData = Reference.instance.score.scoreData;
        _scoreData.OnStars.AddListener((() => _animator.SetTrigger(STARS)));
        
        UIController.OnRestartBtnEvent.AddListener((() => _animator.SetTrigger(RESTART)));
        UIController.OnNextLevelBtnEvent.AddListener((() => _animator.SetTrigger(NEXTLEVEL)));
        UIController.OnExitBtnEvent.AddListener((() => _animator.SetTrigger(EXIT)));
        UIController.OnChooseLevelBtnEvent.AddListener((() => _animator.SetBool(CHOOSELEVEL, !_animator.GetBool(CHOOSELEVEL))));

        
        StateGame.OnEndGame.AddListener((() => _animator.SetBool(_scoreData.stars <= 0? LOST : END,true)));
        StateGame.OnStartGame.AddListener((() => {_animator.SetBool(START, true);}));
        StateGame.OnPlayGame.AddListener((() => {_animator.SetBool(START, false);}));
    }

    private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (!_animator)
            _animator = animator;
    }


    private void OnDestroy()
    {
        _scoreData.OnStars.RemoveListener((() => _animator.SetTrigger(STARS)));
        
        UIController.OnRestartBtnEvent.RemoveListener((() => _animator.SetTrigger(RESTART)));
        UIController.OnNextLevelBtnEvent.RemoveListener((() => _animator.SetTrigger(NEXTLEVEL)));
        UIController.OnExitBtnEvent.RemoveListener((() => _animator.SetTrigger(EXIT)));
        UIController.OnChooseLevelBtnEvent.RemoveListener((() => _animator.SetBool(CHOOSELEVEL, !_animator.GetBool(CHOOSELEVEL))));
        
        StateGame.OnEndGame.RemoveListener((() => _animator.SetBool(_scoreData.stars <= 0? LOST : END,true)));
        StateGame.OnStartGame.RemoveListener((() => {_animator.SetBool(START, true);}));
        StateGame.OnPlayGame.RemoveListener((() => {_animator.SetBool(START, false);}));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBExitState : StateMachineBehaviour
{
    private void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        StateGame.ExitGame();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBNextLevelState : StateMachineBehaviour
{
    private void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        StateGame.NextLevel();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
public class ObjectPlacerInChunk : MonoBehaviour
{
    [MenuItem("Tools/Place in chunk %#F7")]
    private static void PlaceInChunk()
    {
        var objects = Selection.gameObjects;
        var chunks = FindObjectsOfType(typeof(LevelChunk)) as LevelChunk[];
        
        foreach (var obj in objects)
        {
            PlaceInChunk(obj.gameObject, chunks);
        }
    }
    
    private static void PlaceInChunk(GameObject obj, LevelChunk[] chunks)
    {
        foreach (var chunk in chunks)
        {
            var map = chunk.GetComponent<Tilemap>();
            float xMin = map.cellBounds.xMin + map.transform.localPosition.x;
            float xMax = map.cellBounds.xMax + map.transform.localPosition.x;

            if (xMin <= obj.transform.position.x && xMax >= obj.transform.position.x)
            {
                obj.transform.parent = chunk.gameObject.transform;
            }
        }
    }
}
#endif

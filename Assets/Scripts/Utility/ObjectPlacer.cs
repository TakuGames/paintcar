﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class ObjectPlacer : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("Tools/Place to ground %#F6")]
    private static void PlaceToGround()
    {
        var objects = Selection.gameObjects;
        //var objects = FindObjectsOfType(typeof(Painted)) as Painted[];
        
        foreach (var obj in objects)
        {
            BoxCollider2D collider = obj.GetComponent<BoxCollider2D>();
//            AssignCollider(collider);
            Place(collider);
        }
    }

    private static void AssignCollider(BoxCollider2D collider)
    {
        SpriteRenderer sr;
        
        var painted = collider.GetComponent<Colorable>();

        if (painted == null)
            sr = collider.GetComponent<SpriteRenderer>();
        else
            sr = collider.GetComponent<Colorable>().GetSprite;

        collider.size = sr.bounds.size;
        collider.offset = Vector2.zero;
    }
#endif

    public static void Place(Collider2D collider)
    {
        var origin = collider.bounds.center;

        var hit = Physics2D.Raycast(origin, Vector2.down, 10f, LayerMask.GetMask("Platform"));

        if (hit.collider == null)
        {
            return;
        }

        if (hit.distance == 0)
        {
            return;
        }

        var pos = collider.transform.position;
        var height = collider.bounds.max.y - collider.bounds.min.y;
        pos.y -= hit.distance - height * 0.5f;
        collider.transform.position = pos;
    }
}


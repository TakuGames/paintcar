﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Fps : MonoBehaviour
{
    private TextMeshProUGUI _text;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        _text = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        _text.text = "FRAME RATE: \n" + 1.0f / Time.deltaTime;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationController : MonoBehaviour
{
   // private void OnEnable()
   // {
   //     Colorable.OnPanted.AddListener(PaintVibratoMedium);
   // }
   // private void OnDisable()
   // {
   //     Colorable.OnPanted.RemoveListener(PaintVibratoMedium);
   // }

    void PaintVibratoMedium()
    {
        //Debug.Log("Medium vibrato");
        Taptic.Medium();
    }
    public void TakeVibro(int power = 1)
    {
        switch (power)
        {
            case 1:
                Debug.Log("Light vibrato");
                Taptic.Light();
                break;
            case 2:
                Debug.Log("Medium vibrato");
                Taptic.Medium();
                break;
            case 3:
                Debug.Log("Heavy vibrato");
                Taptic.Heavy();
                break;
            default:
                break;
        }

    }
}

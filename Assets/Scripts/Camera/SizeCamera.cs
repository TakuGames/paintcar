﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class SizeCamera : MonoBehaviour
{
    [SerializeField] private float _minSize = 7f;
    [SerializeField] private float _speedAnimation = 5f;
    
    private CinemachineVirtualCamera _camera;
    private CinemachineConfiner _confiner;
    private Transform _target;
    private LevelController _controller;
    private float _maxSize;

    private void Awake()
    {
        _camera = GetComponent<CinemachineVirtualCamera>();
        _confiner = GetComponent<CinemachineConfiner>();
        _controller = FindObjectOfType<LevelController>();
        _target = GameObject.FindWithTag("Player").GetComponent<PlayerController>().targetCamera;
        
        StateGame.OnLoadLevel.AddListener(AssignSize);
        StateGame.OnPlayGame.AddListener(() => StartCoroutine(AnimationOut()));
    }

    private void Start()
    {
        _confiner.enabled = true;
        _camera.enabled = false;
        Camera.main.transform.position = _target.transform.position;
        _camera.enabled = true;
    }

    public void AssignSize()
    {
        float height = _controller.border.top - _controller.border.bottom;
        _maxSize = height * 0.5f;
        _camera.Follow = _target;
        _camera.m_Lens.OrthographicSize = _minSize;
    }
    
    private IEnumerator AnimationOut()
    {
        while (_camera.m_Lens.OrthographicSize <= _maxSize)
        {
            _camera.m_Lens.OrthographicSize += _speedAnimation * Time.deltaTime;
            _camera.m_Lens.OrthographicSize = Mathf.Clamp(_camera.m_Lens.OrthographicSize, 0, _maxSize);
            yield return new WaitForEndOfFrame();
        }
    }

    public void PathCache()
    {
        _confiner.InvalidatePathCache();
    }

    private void OnDestroy()
    {
        StateGame.OnLoadLevel.RemoveListener(AssignSize);
        StateGame.OnPlayGame.RemoveListener(() => StartCoroutine(AnimationOut()));
    }
}
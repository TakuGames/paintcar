﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class AnimationCamera : MonoBehaviour
{
    [SerializeField] private float _minScale = 5f;
    [SerializeField] private float _speedAnimation = 5f;
    private float _maxScale;

    private CinemachineVirtualCamera _camera;

    private void Awake()
    {
        _camera = GetComponent<CinemachineVirtualCamera>();
        StateGame.OnPlayGame.AddListener(() => StartCoroutine(AnimationOut()));
    }

    public void Zoom()
    {
        _camera.m_Lens.OrthographicSize = _minScale;
        Debug.Log(_camera.m_Lens.OrthographicSize);
    }
    
    private IEnumerator AnimationOut()
    {
        while (_camera.m_Lens.OrthographicSize <= _maxScale)
        {
            _camera.m_Lens.OrthographicSize += _speedAnimation * Time.deltaTime;
            _camera.m_Lens.OrthographicSize = Mathf.Clamp(_camera.m_Lens.OrthographicSize, 0, _maxScale);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDestroy()
    {
        StateGame.OnPlayGame.RemoveListener(() => StartCoroutine(AnimationOut()));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = " new Barrel Data", menuName = "Barrel/Data")]
public class BarrelData : ScriptableObject
{
    [SerializeField] public float volume;
}

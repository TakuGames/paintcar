﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelContact : AContact
{
    private Barrel _barrel;

    protected override void Awake()
    {
        base.Awake();
        _barrel = GetComponent<Barrel>();
    }

    protected override void Contact(AContact other)
    {
        other.GetComponent<IСhangePaint>()?.ChangePaint(_barrel.data.volume);
        this.damage.Damage();
    }
}
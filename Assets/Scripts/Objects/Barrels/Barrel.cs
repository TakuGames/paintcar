﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour, IDamage
{
    //[SerializeField] private SpriteRenderer _art;
    [SerializeField] private BarrelData _data;
    private Color _color;

    public BarrelData data => _data;

//    private void OnEnable()
//    {
//        _art.color = new Color(Random.Range(0f,1f), Random.Range(0f,1f), Random.Range(0f,1f), 1);
//    }

    public void Damage()
    {
        gameObject.SetActive(false);
    }
}

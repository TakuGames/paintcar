﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullElement : MonoBehaviour
{
    private PullData _pullData;

    public void CheckIn(PullData pullData)
    {
        _pullData = pullData;
    }

    private void OnDestroy()
    {
        _pullData.RemoveItem(this);
    }
}

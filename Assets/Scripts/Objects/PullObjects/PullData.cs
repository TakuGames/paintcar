﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[CreateAssetMenu(menuName = "Pull Object/Pull Data", fileName = "NewPullData")]
public class PullData : ScriptableObject
{
    [SerializeField] private GameObject[] _itemPrefab;
    private readonly List<PullElement> _elements = new List<PullElement>();

    public int layerMask
    {
        get
        {
            int layer = 0;
            
            for (int i = 0; i < _itemPrefab.Length; i++)
            {
                if (_itemPrefab[i].layer == 1)
                    continue;
                
                layer = layer | 1 << _itemPrefab[i].layer;
            }
            
            return layer;
        }
    }

    public GameObject GetItem(Transform defiant)
    {
        GameObject item = null;

        if (_elements.Count != 0)
        {
            foreach (var element in _elements)
            {
                if (!element.gameObject.activeSelf)
                {
                    item = element.gameObject;
                    break;
                }
            }
        }

        if (item == null)
        {
            item = CreateItem();
        }

        Activate(item, defiant);
        return item;
    }

    private GameObject CreateItem()
    {
        var item = Instantiate(_itemPrefab[Random.Range(0, _itemPrefab.Length)]);
        var element = item.AddComponent<PullElement>();
        element.CheckIn(this);
        _elements.Add(element);
        return item;
    }

    private void Activate(GameObject go, Transform defiant)
    {
        go.transform.position = defiant.position;
        go.transform.parent = defiant.parent;
        go.SetActive(true);
    }

    public void RemoveItem(PullElement element)
    {
        _elements.Remove(element);
    }
}
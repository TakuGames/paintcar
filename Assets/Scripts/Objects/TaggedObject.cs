﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaggedObject : MonoBehaviour
{
    [SerializeField] private ETag _tag;

    public ETag tag
    {
        get => _tag;
        set => _tag = value;
    }

    private void OnDestroy()
    {
        var sr = GetComponent<SpriteRenderer>();
        if (sr != null)
            Destroy(sr);
    }
}

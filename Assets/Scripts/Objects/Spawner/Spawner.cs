﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour//, IActivate
{
    [Header("Reference")]
    [SerializeField] private PullData[] _pullDatas;

    [Header("Setup")]
    [SerializeField] private float _delaySpawn = 10f;
    [SerializeField] private int _amountSpawn = 5;
    [SerializeField] private bool _infinityAmountSpawn = true;

    [Header("Overlap")]
    [SerializeField] private float _radius = 3f;

    private WaitForSeconds _delay;
    private int _mask;

    private void Awake()
    {
        DestroyImmediate(GetComponent<Collider2D>());
        DestroyImmediate(GetComponent<SpriteRenderer>());
        DestroyImmediate(transform.GetChild(0).gameObject);
        
        AssignMask();
    }

//    public void Activate()
//    {
//        StartCoroutine(Spawn());
//    }
    
    public void OnEnable()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        _delay = new WaitForSeconds(_delaySpawn);
        
        yield return new WaitForSeconds(Random.Range(0f, 2f));

        while (true)
        {
            yield return _delay;

            if(IsItemInOverlap())
                continue;

            Generate();
            CheckAmmountSpawn();
        }
    }

    #region Services
    
    private void Generate()
    {
        var objectSpawn = _pullDatas[Random.Range(0, _pullDatas.Length)].GetItem(this.transform);
        PlaceToGround(objectSpawn);
    }
    
    private void CheckAmmountSpawn()
    {
        if (_infinityAmountSpawn)
            return;

        _amountSpawn--;

        if (_amountSpawn <= 0)
            Destroy(gameObject);
    }

    private bool IsItemInOverlap()
    {
        var itemInOverlap = Physics2D.OverlapCircleAll(transform.position, _radius, _mask);
        DebugExtension.DebugCircle(transform.position, new Vector3(0, 0, 1), Color.red, _radius,_delaySpawn * 0.5f);

        if (itemInOverlap.Length != 0)
            return true;

        return false;
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            DebugExtension.DebugCircle(transform.position, new Vector3(0, 0, 1), Color.blue, _radius,
                _delaySpawn * 0.5f);
        }
    }

    private void PlaceToGround(GameObject go)
    {
        var hit = Physics2D.Raycast(go.transform.position, Vector2.down, 10f, LayerMask.GetMask("Platform"));

        if (hit.collider == null)
            return;

        if (hit.distance == 0)
            return;

        var pos = go.transform.position;
        pos.y -= hit.distance - go.GetComponent<BoxCollider2D>().size.y * 0.5f;
        go.transform.position = pos;
    }

    private void AssignMask()
    {
        _mask = 1 << LayerMask.NameToLayer("Player");
        
        foreach (var pull in _pullDatas)
        {
            _mask = _mask | pull.layerMask;
        }
    }
    
    #endregion
}
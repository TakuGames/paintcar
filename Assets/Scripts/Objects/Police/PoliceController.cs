﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceController : MonoBehaviour
{
    private List<GameObject> _polices;
    private ScoreData _scoreConfig;
    
    private void Awake()
    {
        StateGame.OnLoadLevel.AddListener(GenerateCollection);
        _scoreConfig = Reference.instance.score.scoreData;
    }

    private void GenerateCollection()
    {
        _polices = new List<GameObject>(GameObject.FindGameObjectsWithTag("Police"));

        if (_polices.Count == 0)
            Destroy(this);

        foreach (var police in _polices)
        {
            police.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (_scoreConfig.stars < 2)
            return;

        foreach (var police in _polices)
        {
            police.gameObject.SetActive(true);
        }
        
        Destroy(this);
    }

    private void OnDestroy()
    {
        StateGame.OnLoadLevel.RemoveListener(GenerateCollection);
    }
}

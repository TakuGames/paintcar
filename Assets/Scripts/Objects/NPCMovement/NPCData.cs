﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/NPCMovement/Base", fileName = "NewNPCData")]
public class NPCData : ScriptableObject
{
    public float Speed = 2f;
    public LayerMask Mask;
}

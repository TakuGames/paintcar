﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseNPCController : ANPCController
{
    
    private Collider2D _collider;

    private void Start()
    {
        _collider = GetComponent<Collider2D>();

        int i = Random.Range(0, 2);
        
        transform.rotation = Quaternion.Euler(0f, i == 0? 0f : 180f, 0f);
    }

    protected override void Move()
    {
        IsEdge();
        base.Move();
    }

    private void IsEdge()
    {
        var y = _collider.bounds.min.y;
        var x = transform.rotation.y == 0 ? _collider.bounds.max.x : _collider.bounds.min.x;
        
        var hit = Physics2D.Raycast(new Vector2(x, y), Vector2.down, 0.5f, _data.Mask);
        //Debug.DrawLine(new Vector2(x, y), new Vector2(x, y - 0.5f), Color.white, 5f);

        if (hit.collider == null)
            ChangeDirection();
    }

    private void ChangeDirection()
    {
        if (transform.rotation.y == 0)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        else
            transform.rotation = Quaternion.Euler(0f, 0, 0f);
    }

    

    
}

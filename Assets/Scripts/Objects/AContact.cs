﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AContact : MonoBehaviour
{
    protected Collider2D _collider;
    private Rigidbody2D _rb;
    private IDamage _damage;
    
    
    public Collider2D collider => _collider;
    public IDamage damage => _damage;

    public Rigidbody2D rigidbody => _rb;

    protected virtual void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _rb = GetComponent<Rigidbody2D>();
        _damage = GetComponent<IDamage>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        CanContact(other.collider);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        CanContact(other);
    }

    private void CanContact(Collider2D other)
    {
        var contact = other.GetComponent<AContact>();
        if (contact != null)
            Contact(contact);
    }

    protected abstract void Contact(AContact other);
}
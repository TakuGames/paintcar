﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesController : MonoBehaviour
{
    [SerializeField] private int _countSpikesAtTime = 2;
    [SerializeField] private float _delayShowSpikes = 10f;
    private ScoreData _scoreConfig;
    private List<SpikesContact> _spikes;
    private List<SpikesContact> _curSpikes;
    private WaitForSeconds _delay;

    private void Awake()
    {
        StateGame.OnLoadLevel.AddListener(GenerateCollection);
        _scoreConfig = Reference.instance.score.scoreData;
        _scoreConfig.OnStars.AddListener(CheckStars);
    }

    public void GenerateCollection()
    {
        _spikes = new List<SpikesContact>(GetComponentsInChildren<SpikesContact>());

        if (_spikes.Count == 0)
            return;

        foreach (var spike in _spikes)
        {
            ObjectPlacer.Place(spike.collider);
            spike.gameObject.SetActive(false);
        }
    }

    private void CheckStars()
    {
        if (_scoreConfig.stars == 1)
            StartCoroutine(ActivateSpikes());
    }

    private IEnumerator ActivateSpikes()
    {
        _curSpikes = new List<SpikesContact>();
        _delay = new WaitForSeconds(_delayShowSpikes);

        while (true)
        {
            Debug.Log("generate");

            if (_curSpikes.Count != 0)
            {
                foreach (var spike in _curSpikes)
                {
                    spike.gameObject.SetActive(false);
                    _spikes.Add(spike);
                }

                _curSpikes.Clear();
            }

            for (int i = 0; i < _countSpikesAtTime; i++)
            {
                int r = Random.Range(0, _spikes.Count);
                var spike = _spikes[r];
                _spikes.Remove(spike);
                _curSpikes.Add(spike);
                spike.gameObject.SetActive(true);
            }

            yield return _delay;
        }
    }

    private void OnDestroy()
    {
        StateGame.OnLoadLevel.RemoveListener(GenerateCollection);
    }
}

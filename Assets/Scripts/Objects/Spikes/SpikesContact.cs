﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesContact : AContact
{
    [SerializeField] private float _appearanceCheckTime = 5f;
    [SerializeField] private float _distanceToPlayer = 5f;
    private bool _isDamage = false;
    private SpriteRenderer _sr;

    private void OnEnable()
    {
        _sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
        _sr.color = new Color(0, 0, 0, 0);
        StartCoroutine(Activate());
    }

    private IEnumerator Activate()
    {
        var delay = new WaitForSeconds(_appearanceCheckTime);
        var player = Reference.instance.player.gameObject.transform;
        var sqrDistanceCompare = _distanceToPlayer * _distanceToPlayer;
        while (true)
        {
            yield return delay;
            var sqrDistanceToPlayer = Vector2.SqrMagnitude(player.position - transform.position);
            if (sqrDistanceToPlayer > sqrDistanceCompare)
            {
                _sr.color = Color.white;
                _isDamage = true;
                yield break;
            }
        }
    }

    protected override void Contact(AContact other)
    {
        if (_isDamage)
            other.damage.Damage();
    }
}
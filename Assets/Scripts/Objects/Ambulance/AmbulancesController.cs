﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbulancesController : MonoBehaviour
{
    [SerializeField] private float _timeBetweenEnable = 10f;
    [SerializeField] private float _lifeTime = 20f;

    private List<Ambulance> _ambulances;

    private WaitForSeconds _waitTimeBetweenEnable;
    private WaitForSeconds _waitLifeTime;

    private void Awake()
    {
        StateGame.OnLoadLevel.AddListener(GenerateCollection);
    }

    public void GenerateCollection()
    {
        _waitTimeBetweenEnable = new WaitForSeconds(_timeBetweenEnable);
        _waitLifeTime = new WaitForSeconds(_lifeTime);
        
        _ambulances = new List<Ambulance>(GetComponentsInChildren<Ambulance>());

        if (_ambulances.Count == 0)
            return;

        foreach (var ambulance in _ambulances)
        {
            ambulance.Generate(_waitLifeTime, this);
            ambulance.gameObject.SetActive(false);
        }

        HealthPlayer.OnHealthChange.AddListener(CheckHealth);
    }

    private void CheckHealth(int value)
    {
        if (value > 1)
            return;

        Place();
    }

    public void StartWait()
    {
        StartCoroutine(ActivateAmbulance());
    }

    public IEnumerator ActivateAmbulance()
    {
        yield return _waitTimeBetweenEnable;
        Place();
    }


    private void OnDestroy()
    {
        StateGame.OnLoadLevel.RemoveListener(GenerateCollection);
        HealthPlayer.OnHealthChange.AddListener(CheckHealth);
    }

    private void Place()
    {
        _ambulances[Random.Range(0, _ambulances.Count)].Enable();
    }

    public void DestroyAllAmbulance()
    {
        foreach (var obj in _ambulances)
        {
            Destroy(obj.gameObject);
        }
        
        Destroy(this);
    }
}
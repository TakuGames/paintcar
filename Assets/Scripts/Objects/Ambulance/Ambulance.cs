﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ambulance : MonoBehaviour
{
    private AmbulancesController _controller;
    private WaitForSeconds _lifeTime;

    public AmbulancesController controller => _controller;

    public void Generate(WaitForSeconds lifeTime, AmbulancesController controller)
    {
        _lifeTime = lifeTime;
        _controller = controller;
    }
    
    public void Enable()
    {
        Debug.Log("Start Ambulance");
        gameObject.SetActive(true);
        StartCoroutine(LifeCycle());
    }

    private IEnumerator LifeCycle()
    {
        yield return _lifeTime;
        Debug.Log("End Ambulance");
        _controller.StartWait();
        gameObject.SetActive(false);
    }
}

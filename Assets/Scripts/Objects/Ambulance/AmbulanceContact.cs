﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbulanceContact : AContact
{
    protected override void Contact(AContact other)
    {
        Reference.instance.player.config.health.curHealth += 1;
        GetComponent<Ambulance>().controller.DestroyAllAmbulance();
    }
}
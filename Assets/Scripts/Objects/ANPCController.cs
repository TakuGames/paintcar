﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ANPCController : MonoBehaviour
{
    [SerializeField] protected NPCData _data;


    // Update is called once per frame
    void Update()
    {
        Move();
    }

    protected virtual void Move()
    {
        transform.Translate(new Vector3(_data.Speed, 0f, 0f) * Time.deltaTime);
        if (transform.position.y < -10f)
            gameObject.SetActive(false);
    }

    public virtual void PlaceInChunk(LevelChunk chunk)
    {
        if (gameObject.activeSelf)
            gameObject.transform.parent = chunk.gameObject.transform;
    }
}

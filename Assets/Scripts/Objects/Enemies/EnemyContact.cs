﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContact : AContact
{
    private List<ContactPoint2D> _contacts;
    private ContactFilter2D _filter;

    private void Start()
    {
        _contacts = new List<ContactPoint2D>();
        _filter = new ContactFilter2D();
        _filter.useLayerMask = true;
        _filter.layerMask = 1 << LayerMask.NameToLayer("Player");  
    }

    protected override void Contact(AContact other)
    {
        if (IsDamage())
        {
            this.damage.Damage();
        }
        else
        {
            other.damage.Damage();
        }
    }

    private bool IsDamage()
    {
        rigidbody.GetContacts(_filter, _contacts);
        foreach (var point in _contacts)
        {
            if (point.normal.x == 0)
                return true;
        }

        return false;
    }
}



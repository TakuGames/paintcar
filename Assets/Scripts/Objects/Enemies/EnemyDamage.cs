﻿using UnityEngine;
using UnityEngine.Events;

public class EnemyDamage : MonoBehaviour, IDamage
{
    public void Damage()
    {
        Reference.instance.effectsController.UseEffectOnce(EFX.enemyDeath, transform);
        gameObject.SetActive(false);
        gameObject.transform.parent = null;
        Reference.instance.score.scoreData.deadEnemies++; 
        Debug.Log("Damage");
    }
}

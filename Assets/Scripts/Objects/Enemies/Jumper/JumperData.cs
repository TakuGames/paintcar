﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Data/EnemyData/Jumper", fileName = "NewEnemyJumperData")]
public class JumperData : NPCData
{
    [SerializeField] public float TimeBetweenJumps;
    [SerializeField] public float TimeBetweenRotations;
    [SerializeField] public float JumpForce;
    [SerializeField] public ForceMode2D ForceMode;
}

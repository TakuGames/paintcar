﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperEnemyController : ANPCController
{
    private JumperData _jData;

    private Rigidbody2D _rigidbody;
    private float _curJumpTime = 0f;
    private float _curRotateTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        if (!(_data is JumperData))
            throw new ArgumentException("_data is not JumperData;  JumperEnemyController; 18");

        _jData = (JumperData) _data;

        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    protected override void Move()
    {
        _curJumpTime += Time.deltaTime;
        _curRotateTime += Time.deltaTime;

        if (_jData.TimeBetweenJumps < _curJumpTime)
            Jump();

        if (_jData.TimeBetweenRotations < _curRotateTime)
            ChangeDirection();

        base.Move();
    }

   

    private void Jump()
    {
        _curJumpTime = 0f;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up,2f, _jData.Mask);

        if (!hit.collider) return;
        
        _rigidbody.AddForce(new Vector2(0, _jData.JumpForce), _jData.ForceMode);
    }

    private void ChangeDirection()
    {
        _curRotateTime = 0f;

        if (transform.rotation.y == 0)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        else
            transform.rotation = Quaternion.Euler(0f, 0, 0f);
    }
}

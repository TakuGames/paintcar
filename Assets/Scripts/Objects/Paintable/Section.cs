﻿using UnityEngine;
using System;

[Serializable]
public struct Section
{
    public int Index;
    public float xMin;
    public float xMax;
    public float xMinNorm;
    public float xMaxNorm;
    public Color color; 

    public Section(float min = 0, float max = 0, int number = 0, float minNorm = 0, float maxNorm = 0 , Color color = new Color())
    {
        xMin = min;
        xMax = max;
        Index = number;
        xMinNorm = minNorm;
        xMaxNorm = maxNorm;
        this.color = color;
    }
}

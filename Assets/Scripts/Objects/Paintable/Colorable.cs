﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class PaintedEvent : UnityEvent { }

public class Colorable : MonoBehaviour
{
    public Action<float> OnGetFill = (float fill) => { };
    public static PaintedEvent OnPanted = new PaintedEvent();

    public bool IsAssignCurrentSection => _isAssignCurrentSection;
    public float GetWight => _width;
    public BoxCollider2D GetCollider => _collider;
    public SpriteRenderer GetSprite => _grayscale;

    [SerializeField] private SpriteRenderer _grayscale;

    private bool _isAssignCurrentSection = false;

    private float _minBound;
    private float _maxBound;
    private float _score;
    private float _width;
    private float _minDistFullPaint;

    private BoxCollider2D _collider;
    private PaintController _player;
    private SetSectionsInShader _shader;

    private Section _currentSection;
    private List<Section> _sections = new List<Section>();

    private void Start()
    {
        if (!_grayscale)
        {
            Debug.Log("Sprite renderer must be added in " + gameObject.name);
            return;
        }

        _shader = GetComponent<SetSectionsInShader>();
        _collider = GetComponent<BoxCollider2D>();

        _width = _collider.bounds.size.x;

        _minBound = _width * -0.5f;
        _maxBound = _width * 0.5f;

        _grayscale.gameObject.SetActive(true);

        _minDistFullPaint = Reference.instance.player.config.paint.minPixelsToFullFill / _grayscale.sprite.rect.width;

        _collider.isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;

        if (!_player)
            _player = collision.GetComponent<PaintController>();

        if (!_player)
            return;

        _player.AddPainted(this);

        if (_score < 1 && _score >= 1 - _minDistFullPaint)
        {
            ComplitePainting();
        }
        if (_score >= 1)
        {
            OnPanted.Invoke();
            Destroy(_collider);
            Destroy(_shader);
            Destroy(this);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;

        if (_score < 1 && _score >= 1 - _minDistFullPaint)
        {
            ComplitePainting();
        }
        if (_score >= 1)
        {
            OnPanted.Invoke();
            Destroy(_collider);
            Destroy(_shader);
            Destroy(this);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;

        if (_player)
            _player.DeletePainted(this);

        _isAssignCurrentSection = false;

        if (_score < 1 && _score >= 1 - _minDistFullPaint)
        {
            ComplitePainting();
        }
        if (_score >= 1)
        {
            OnPanted.Invoke();
            Destroy(_collider);
            Destroy(_shader);
            Destroy(this);
        }
    }

    public bool IsEnterTrigger(float fillerPosition)
    {
        if (fillerPosition < _minBound || fillerPosition > _maxBound)
            return false;

        return true;
    }

    public void AssignCurrentSection(float fillerPositionBefore, float fillerPosition, EDirection direction, float distancePerFrame = 0)
    {
        if (direction == EDirection.Right)
        {
            if ((fillerPositionBefore) < _minBound)
                fillerPosition = _minBound;
        }
        if (direction == EDirection.Left)
        {
            if ((fillerPositionBefore) > _maxBound)
                fillerPosition = _maxBound;
        }

        if (_sections.Count == 0)
        {
            _currentSection = new Section(fillerPosition, fillerPosition, 0);
            _sections.Add(_currentSection);
            _isAssignCurrentSection = true;
            return;
        }

        foreach (var section in _sections)
        {
            if (section.xMin <= fillerPosition && section.xMax >= fillerPosition)
            {
                _currentSection = section;
                _isAssignCurrentSection = true;
                return;
            }
        }

        int index = 0;

        for (index = 0; index < _sections.Count; index++)
        {
            if (_sections[index].xMax >= fillerPosition)
                break;
        }

        if (index >= _sections.Count)
            index = _sections.Count;

        for (int i = index; i < _sections.Count; i++)
        {
            var sec = _sections[i];
            sec.Index++;
            _sections[i] = sec;
        }

        _currentSection = new Section(fillerPosition, fillerPosition, index);
        _sections.Insert(index, _currentSection);
        _isAssignCurrentSection = true;
    }
    public void MergeSections(float fillerPosition, EDirection direction)
    {
        if (direction == EDirection.Right)
        {
            if (_currentSection.Index >= _sections.Count - 1)
                return;

            var nextSection = _sections[_currentSection.Index + 1];

            if (nextSection.xMin <= fillerPosition)
            {
                var sec = _currentSection;
                sec.xMax = nextSection.xMax;
                _sections[_currentSection.Index] = sec;
                _currentSection = sec;
                _sections.Remove(nextSection);

                ChangeIndexes();
            }
        }
        if (direction == EDirection.Left)
        {
            if (_currentSection.Index <= 0)
                return;

            var nextSection = _sections[_currentSection.Index - 1];

            if (nextSection.xMax >= fillerPosition)
            {
                var sec = nextSection;
                sec.xMax = _currentSection.xMax;
                _sections[nextSection.Index] = sec;
                nextSection = sec;
                _sections.Remove(_sections[_currentSection.Index]);
                _currentSection = nextSection;

                ChangeIndexes();
            }
        }
    }
    public void ChangeBorders(float fillerPosition, EDirection direction, float distancePerFrame = 0)
    {
        var sec = _currentSection;

        if (direction == EDirection.Right)
        {
            if (fillerPosition > _currentSection.xMax)
            {
                sec.xMax = (fillerPosition) >= _maxBound
                    ? _maxBound
                    : fillerPosition;
            }
        }
        if (direction == EDirection.Left)
        {
            if (fillerPosition < _currentSection.xMin)
            {
                sec.xMin = (fillerPosition) <= _minBound
                    ? _minBound
                    : fillerPosition;
            }
        }

        sec.xMin = Mathf.Clamp(sec.xMin, _minBound, _maxBound);
        sec.xMax = Mathf.Clamp(sec.xMax, _minBound, _maxBound);

        _sections[_currentSection.Index] = sec;
        _currentSection = sec;
    }
    public void PaintShader()
    {
        for (int i = 0; i < _sections.Count; i++)
        {
            var sec = _sections[i];
            var min = (sec.xMin + (_width * 0.5f)) / _width;
            var max = (sec.xMax + (_width * 0.5f)) / _width;

            sec.xMinNorm = (float)Math.Round(min, 3, MidpointRounding.AwayFromZero);
            sec.xMaxNorm = (float)Math.Round(max, 3, MidpointRounding.AwayFromZero);
            _sections[i] = sec;
        }

        _shader.SetSections = _sections;
    }

    public float CalculateDistanceFilling()
    {
        float curScore = 0;

        foreach (var item in _sections)
        {
            curScore += (item.xMaxNorm - item.xMinNorm);
        }

        if (curScore >= 1)
            curScore = 1;

        float expense = curScore - _score;
        OnGetFill(expense);
        _score = curScore;

        return expense;
    }

    private void ChangeIndexes()
    {
        for (int i = _currentSection.Index + 1; i < _sections.Count; i++)
        {
            var sec = _sections[i];
            sec.Index--;
            _sections[i] = sec;
        }
    }
    private void ComplitePainting()
    {
        var sec = _currentSection;
        sec.xMin = _minBound;
        sec.xMax = _maxBound;
        _currentSection = sec;

        Reference.instance.effectsController.UseEffectOnce(EFX.painted , transform);
    }

    [ContextMenu("Assign Collider")]
    private void AssignCollider()
    {
        var collider = GetComponent<BoxCollider2D>();

        collider.size = _grayscale.bounds.size;
        collider.offset = Vector2.zero;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PlayerController))]
public class PlayerControllerEditor : Editor
{
    private SerializedProperty _playerConfigProp;
    private SerializedProperty _playerFillerProp;
    private SerializedProperty _playerTargetCameraProp;
    private SerializedProperty _playerBaseSRProp;
    private SerializedObject _serializedObject;
    private Editor _playerConfigEditor;

    private bool _referencesFoldout;
    private bool _configFoldout;

    private PlayerController _playerController;

    void OnEnable()
    {
        _playerConfigProp = serializedObject.FindProperty("_config");
        _playerFillerProp = serializedObject.FindProperty("_filler");
        _playerTargetCameraProp = serializedObject.FindProperty("_targetCamera");
        _playerBaseSRProp = serializedObject.FindProperty("_baseSR");
        //_playerController = target as PlayerController;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        _playerConfigEditor = Editor.CreateEditor(_playerConfigProp.objectReferenceValue);
        
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;

        _referencesFoldout = EditorGUILayout.Foldout(_referencesFoldout, new GUIContent("Reference"));

        if (_referencesFoldout)
        {
            EditorGUILayout.PropertyField(_playerConfigProp, new GUIContent("Config"));
            EditorGUILayout.PropertyField(_playerFillerProp, new GUIContent("Filler"));
            EditorGUILayout.PropertyField(_playerTargetCameraProp, new GUIContent("Target Camera"));
            EditorGUILayout.PropertyField(_playerBaseSRProp, new GUIContent("Base SR"));
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
        
        
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;
        
        _configFoldout = EditorGUILayout.Foldout(_configFoldout, new GUIContent("Config"));
        
        if (_configFoldout)
        {
            if (_playerConfigEditor != null)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                _playerConfigEditor.OnInspectorGUI();
                EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("Player Config no link!", MessageType.Error);
            }
        }
        
        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
        

//        if (_playerConfigEditor != null)
//        {
//            _serializedObject = new SerializedObject(_config.config);
//            PropertyBox(ref _controllerFoldout, new GUIContent("Controller"), new [] {"move", "input", "jump"});
//            PropertyBox(ref _healthFoldout, new GUIContent("Health"), new [] {"health"});
//        }

        serializedObject.ApplyModifiedProperties();
    }

    private void PropertyBox(ref bool foldout, GUIContent content, string[] names)
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;
        
        foldout = EditorGUILayout.Foldout(foldout, content);

        if (foldout)
        {
            foreach (var name in names)
            {
                SerializedProperty sp = _serializedObject.FindProperty(name);
                EditorGUILayout.PropertyField(sp, true);
            }
        }
        
        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }
}


//GUILayout.BeginVertical(EditorStyles.helpBox);
//EditorGUI.BeginDisabledGroup(true);
//_playerConfigEditor.OnInspectorGUI();
//EditorGUI.EndDisabledGroup(); 
//EditorGUILayout.EndVertical();
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckStuck : MonoBehaviour
{
    private Collider2D _collider;
    private Rigidbody2D _rb;
    private int _mask;
    private int _layer;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }

    private void Start()
    {
        _layer = LayerMask.NameToLayer("Platform");
        _mask = 1 << _layer;
    }

    public void Check()
    {
        DebugExtension.DebugBounds(new Bounds(_collider.bounds.center, _collider.bounds.size), Color.blue, 5f);
        
        var topPoint = new Vector2(_collider.bounds.center.x, _collider.bounds.max.y);
        var hitDown = Physics2D.Raycast(topPoint, Vector2.down, _collider.bounds.size.y, _mask);
        
        if (!hitDown)
            return;
        
        AddHeight(_collider.bounds.size.y - hitDown.distance);
        
        DebugExtension.DebugBounds(new Bounds(_collider.bounds.center, _collider.bounds.size), Color.green, 5f);
    }

    private void AddHeight(float height)
    {
        Vector2 pos = _rb.position;
        pos.y += height;
        _rb.position = pos;
        transform.position = pos;
    }
    
    
//    public void Check()
//    {
//        DebugExtension.DebugBounds(new Bounds(_collider.bounds.center, _collider.bounds.size), Color.blue, 5f);
//        
//        var bottomPoint = new Vector2(_collider.bounds.center.x, _collider.bounds.min.y);
//        var hitUp = Physics2D.Raycast(bottomPoint, Vector2.up, _collider.bounds.size.y, _mask);
//        
//        if (!hitUp)
//            return;
//        
//        AddHeight(hitUp.distance);
//        
//        DebugExtension.DebugBounds(new Bounds(_collider.bounds.center, _collider.bounds.size), Color.green, 5f);
//    }
    
    

//    private void Check(Collision2D other, Color color = default(Color))
//    {
//        if (_collider.isTrigger)
//            return;
//        
//        if (other.gameObject.layer != _layer)
//            return;
//        
//        //Debug.Log(other.gameObject.name);
//        
//        var bottomPoint = new Vector2(_collider.bounds.center.x, _collider.bounds.min.y);
//        var hitUp = Physics2D.Raycast(bottomPoint, Vector2.up, _collider.bounds.size.y, _mask);
//
//        if (hitUp)
//        {
//            Debug.DrawLine(bottomPoint, hitUp.point, color, 2f);
//        }
//        else
//        {
//            Debug.DrawLine(bottomPoint, bottomPoint  + Vector2.up * _collider.bounds.size.y, Color.red, 2f);
//        }
//        
//
//        if (!hitUp)
//            return;
//
//        if (hitUp.distance > _collider.bounds.size.y)
//        {
//            Debug.Log("01");
//            AddHeight(hitUp.distance);
//            return;
//        }
//
//        var highPoint = new Vector2(_collider.bounds.center.x, _collider.bounds.max.y);
//        var hitDown = Physics2D.Raycast(highPoint, Vector2.down, _collider.bounds.size.y, _mask);
//        var hitUpOnHighPoint = Physics2D.Raycast(highPoint, Vector2.up, _collider.bounds.size.y, _mask);
//
//        if (hitDown)
//        {
//            Debug.DrawLine(new Vector2(highPoint.x - 0.5f, highPoint.y), new Vector2(hitDown.point.x - 0.5f, hitDown.point.y));
//        }
//        else
//        {
//            Debug.DrawLine(new Vector2(highPoint.x - 0.5f, highPoint.y), new Vector2(highPoint.x - 0.5f, highPoint.y)  + Vector2.down * _collider.bounds.size.y, Color.red, 2f);
//        }
//
//        if (hitUpOnHighPoint)
//        {
//            Debug.DrawLine(new Vector2(highPoint.x + 0.5f, highPoint.y), new Vector2(hitUpOnHighPoint.point.x + 0.5f, hitUpOnHighPoint.point.y), Color.blue, 2f);
//        }
//        else
//        {
//            Debug.DrawLine(new Vector2(highPoint.x + 0.5f, highPoint.y), new Vector2(highPoint.x + 0.5f, highPoint.y)  + Vector2.up * _collider.bounds.size.y, Color.blue, 2f);
//        }
//
//        if (hitUpOnHighPoint)
//        {
//            Debug.Log("02");
//            AddHeight(hitUp.distance + hitDown.distance + hitUpOnHighPoint.distance);
//        }
//        else
//        {
//            Debug.Log("03");
//            AddHeight(_collider.bounds.size.y - hitDown.distance);
//        }
//    }
}
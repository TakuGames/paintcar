﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Gun : MonoBehaviour
{
    [FormerlySerializedAs("_soGun")] [SerializeField] private GunData gunData;
    [SerializeField] private Transform _bulletSpawn;

    private WaitForSeconds _delayRecharge;
    private PaintController _paintController;


    void Awake()
    {
        _paintController = GetComponentInParent<PaintController>();
        _delayRecharge = new WaitForSeconds(gunData.TimeBetweenShots);
        gunData.bulletDataConfig.Speed = GetComponentInParent<PlayerController>().config.move.maxSpeed + gunData.AddedBulletSpeedFromPlayer;
    }

//    private void Start()
//    {
//        StartCoroutine(Shot());  
//    }
//
//
//    private IEnumerator Shot()
//    {
//        while (true)
//        {
//            if (!_paintController.isHavePaint(gunData.bulletDataConfig.PaintLiquidToShot))
//            {
//                yield return _delayRecharge;
//                continue;
//            }
//
//            RaycastHit2D hit = Physics2D.Raycast(_bulletSpawn.position, _bulletSpawn.transform.right, gunData.DistanceToAim, gunData.Mask);
//            Debug.DrawLine(_bulletSpawn.position, new Vector2(_bulletSpawn.position.x + (gunData.DistanceToAim * _bulletSpawn.transform.right.x), _bulletSpawn.position.y), Color.red, 2f);
//            if (hit.collider != null)
//            {
//                if (hit.collider.tag == "Enemy")
//                {
//                    var bulletObject = PullObjects.GetBullet(_bulletSpawn);
//                    bulletObject.transform.rotation = transform.parent.transform.rotation;
//                    
//                    _paintController.ChangePaint(-gunData.bulletDataConfig.PaintLiquidToShot);
//                }
//            }
//            yield return _delayRecharge;
//        }
//    }
}

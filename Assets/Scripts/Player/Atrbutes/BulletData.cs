﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new BulletSettings", menuName = "GunSettings/BulletSettings")]
public class BulletData : ScriptableObject
{
    [SerializeField] public float TimeToDeleteBullet;
    [SerializeField] public float PaintLiquidToShot;
    private float _speed;
    private Color _color;

    public float Speed { get => _speed; set => _speed = value; }
    public Color Color { get => _color; set => _color = value; }
}

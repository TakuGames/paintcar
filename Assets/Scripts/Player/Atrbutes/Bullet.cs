﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(SpriteRenderer))]
public class Bullet : MonoBehaviour
{

    [FormerlySerializedAs("_soBullet")] [SerializeField] private BulletData bulletData;
    private Vector3 _velocity;
    private SpriteRenderer _spriteRenderer;
    
    public float Velocity {set => _velocity = new Vector3(value,0,0); }
    

    private WaitForSeconds _delayDeleteTime;
    private void Awake()
    {
        _delayDeleteTime = new WaitForSeconds(bulletData.TimeToDeleteBullet);
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        _spriteRenderer.color = bulletData.Color;
        StartCoroutine(DeleteCoroutine());
        Velocity = bulletData.Speed;
    }

    void Update()
    {
        
        transform.Translate(_velocity * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag != "Enemy") return;
        var damageObject = other.GetComponent<IDamage>();
        if (damageObject == null) return;
        damageObject.Damage();
        //StopAllCoroutines();
        
        gameObject.SetActive(false);
    }

    private IEnumerator DeleteCoroutine()
    {
        yield return _delayDeleteTime;
        gameObject.SetActive(false);
    }


    
}

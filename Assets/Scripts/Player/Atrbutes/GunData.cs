﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "new GunSettings" , menuName = "GunSettings/GunSettings")]
public class GunData : ScriptableObject
{
    [SerializeField] public float DistanceToAim;
    [SerializeField] public float TimeBetweenShots;
    [SerializeField] public float AddedBulletSpeedFromPlayer;
    [SerializeField] public LayerMask Mask;
    [FormerlySerializedAs("SOBulletConfig")] [SerializeField] public BulletData bulletDataConfig;
    
}

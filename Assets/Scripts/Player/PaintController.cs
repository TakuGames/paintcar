﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerController))]

public class PaintController : MonoBehaviour, IСhangePaint
{
    public bool isPanting => _isPainting;

    private Transform _filler;
    private PlayerConfig _config;
    private List<Colorable> _painted = new List<Colorable>();

    private float _curPaint = 0;
    private float _offsetFiller;

    private bool _isPainting;

    private void Awake()
    {
        _config = Reference.instance.player.config;
        _config.paint.curPaint = _config.paint.startPaint;
        _config.paint.OnPaintChange = new UnityEvent();
    }

    private void Start()
    {
        _filler = GetComponent<PlayerController>().filler;
        _offsetFiller = _filler.localPosition.x;
    }

    private void LateUpdate()
    {
        CheckPainting();

        if (_painted.Count <= 0)
            return;

        if (!isHavePaint())
            return;

        foreach (var item in _painted)
        {
            var fillerLocalPositionBefore = transform.position.x - _config.move.distancePerFrame + _offsetFiller - item.transform.position.x;
            var fillerLocalPosition = _filler.transform.position.x - item.transform.position.x;

            if (!item.IsAssignCurrentSection)
                item.AssignCurrentSection(fillerLocalPositionBefore, fillerLocalPosition, _config.move.direction, _config.move.distancePerFrame);

            item.MergeSections(fillerLocalPosition, _config.move.direction);
            item.ChangeBorders(fillerLocalPosition, _config.move.direction, _config.move.distancePerFrame);
            item.PaintShader();
            ChangePaint(-item.CalculateDistanceFilling() * _config.paint.expensePaint);
        }
    }

    public void AddPainted(Colorable colorable)
    {
        _painted.Add(colorable);
    }
    public void DeletePainted(Colorable colorable)
    {
        _painted.Remove(colorable);
    }
    public void ChangePaint(float value)
    {
        _config.paint.curPaint = Mathf.Clamp(_config.paint.curPaint + value, 0, _config.paint.maxPaint);
        _config.paint.OnPaintChange.Invoke();
    }
    public bool isHavePaint(float need = 0)
    {
        if (_config.paint.curPaint > need)
            return true;

        return false;
    }


    private void CheckPainting()
    {
        if (_config.paint.expensePaint <= 0)
        {
            _isPainting = _painted.Count > 0;
            return;
        }

        
        if (_curPaint > _config.paint.curPaint)
        {
            _isPainting = true;
        }
        else
        {
            _isPainting = false;
        }
        
        _curPaint = _config.paint.curPaint;
    }
}


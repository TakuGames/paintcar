﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCamera : MonoBehaviour
{
    [SerializeField] private float _offset = 1f;
    private Transform _targetCam;
    private float _halfWidth;

    private void Awake()
    {
        _targetCam = Reference.instance.player.targetCamera;
        StateGame.OnPlayGame.AddListener(OffsetPosition);
    }

    private void Start()
    {
        _halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
        _targetCam.localPosition = new Vector3(Reference.instance.player.config.move.maxSpeed / 3,0,0);
    }



    private void OffsetPosition()
    {
        Vector2 newLocPos = _targetCam.localPosition;
        newLocPos.x = _halfWidth - _offset;
        _targetCam.localPosition = newLocPos;
        StateGame.OnPlayGame.RemoveListener(OffsetPosition);
    }


}

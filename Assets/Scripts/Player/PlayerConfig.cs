﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.Serialization;


public class HealthChangeEvent : UnityEvent<int> { }

[Serializable]
public struct MovePlayer
{
    public float maxSpeed;
    [Range(0, 10)] public float acceleration;
    public bool accelerationEnable;
    public bool blockDirection;

    private EDirection _direction;
    private float _curSpeed;
    private float _distancePerFrame;

    public float curSpeed
    {
        get => _curSpeed;
        set => _curSpeed = value;
    }

    public EDirection direction
    {
        get => _direction;
        set => _direction = value;
    }
    
    public float distancePerFrame
    {
        get => _distancePerFrame;
        set => _distancePerFrame = value;
    }
}

[Serializable]
public struct JumpPlayer
{
    public float jumpForce;
    public float groundDelay;
    public bool isJump;
    public bool isDown;
    public LayerMask mask;
    public float velocityLimit;
}

[Serializable]
public struct InputPlayer
{
    public bool isInput;
    public List<Vector2> _input;

    public Vector2 input
    {
        get
        {
            if(_input == null)
                _input = new List<Vector2>();
            
            if (_input.Count == 0)
                return Vector2.zero;
            
            var curInput = _input[0];
            _input.RemoveAt(0);
            return curInput;
        }

        set
        {
            if (_input.Count >= 2)
                return;
            
            _input.Add(value);
        }
    }
}

[Serializable]
public struct HealthPlayer
{
    public int startHealth;
    public float timeDamageEffect;
    public float delayBlinking;
    private int _curHealth;
    public int curHealth
    {
        get => _curHealth;
        set
        {
            _curHealth = value;
            OnHealthChange.Invoke(_curHealth);
        }
    }
    
    public static HealthChangeEvent OnHealthChange = new HealthChangeEvent();
    public static UnityEvent OnDeath = new UnityEvent();
}

[Serializable]
public struct PaintPlayer
{
    public float maxPaint;
    public float startPaint;
    public float expensePaint;
    public float minPixelsToFullFill;
    private float _curPaint;
    public float curPaint
    {
        get => _curPaint;
        set => _curPaint = value;
    }
    
    public UnityEvent OnPaintChange;
}

[Serializable]
public struct ContactPlayer
{
    public string enemyTag;
    public string barrelTag;
}


[CreateAssetMenu(fileName = "PlayerConfig", menuName = "Player/Config")]
public class PlayerConfig : ScriptableObject
{
    public MovePlayer move;
    public InputPlayer input;
    public JumpPlayer jump;
    public HealthPlayer health;
    public PaintPlayer paint;
    public ContactPlayer contact;
}
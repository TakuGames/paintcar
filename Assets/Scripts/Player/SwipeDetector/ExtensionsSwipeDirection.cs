﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionsSwipeDirection 
{
    public static SwipeDirection Next(this SwipeDirection curr)
    {
        switch (curr)
        {
            case SwipeDirection.Up:
                return SwipeDirection.Right;
            case SwipeDirection.Down:
                return SwipeDirection.Left;
            case SwipeDirection.Left:
                return SwipeDirection.Up;
            case SwipeDirection.Right:
                return SwipeDirection.Down;
            default:
                return SwipeDirection.Down;
        }
    }

    public static SwipeDirection Prev(this SwipeDirection curr)
    {
        switch (curr)
        {
            case SwipeDirection.Up:
                return SwipeDirection.Left;
            case SwipeDirection.Down:
                return SwipeDirection.Right;
            case SwipeDirection.Left:
                return SwipeDirection.Down;
            case SwipeDirection.Right:
                return SwipeDirection.Up;
            default:
                return SwipeDirection.Down;
        }
    }

    public static SwipeDirection Opposite(this SwipeDirection curr)
    {
        switch (curr)
        {
            case SwipeDirection.Up:
                return SwipeDirection.Down;
            case SwipeDirection.Down:
                return SwipeDirection.Up;
            case SwipeDirection.Left:
                return SwipeDirection.Right;
            case SwipeDirection.Right:
                return SwipeDirection.Left;
            default:
                return SwipeDirection.Down;
        }
    }

    public static SwipeDirection? Opposite(this SwipeDirection? curr)
    {
        switch (curr)
        {
            case SwipeDirection.Up:
                return SwipeDirection.Down;
            case SwipeDirection.Down:
                return SwipeDirection.Up;
            case SwipeDirection.Left:
                return SwipeDirection.Right;
            case SwipeDirection.Right:
                return SwipeDirection.Left;
            default:
                return null;
        }
    }
}

﻿using System;
using UnityEngine;

public class SwipeDetector : ASwipeDetector
{
    

    protected override void DetectSwipe()
    {
        if (SwipeDistanceCheck())
        {
            if (!_oneSwipeAtTime)
                return;

            if (IsVerticalSwipe())
            {
                var direction = _fingerDownPosition.y - _fingerUpPosition.y > 0 ? SwipeDirection.Down : SwipeDirection.Up;
                SendSwipe(direction);
            }
            else
            {
                var direction = _fingerDownPosition.x - _fingerUpPosition.x > 0 ? SwipeDirection.Left : SwipeDirection.Right;
                SendSwipe(direction);
            }

            _fingerUpPosition = _fingerDownPosition;

            if (_oneSwipeAtTime)
                _oneSwipeAtTime = false;
        }
    }

}


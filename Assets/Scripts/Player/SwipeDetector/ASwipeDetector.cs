﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASwipeDetector : MonoBehaviour
{
    public float DistanceForSwipe { get { return _minDistanceForSwipe; } set { _minDistanceForSwipe = value; } }

    public static event Action<SwipeData> OnSwipe = delegate { };

    [SerializeField] private Camera _cam;
    [SerializeField] private float _minDistanceForSwipe = 1;

    protected Vector2 _fingerDownPosition;
    protected Vector2 _fingerUpPosition;

    protected bool _oneSwipeAtTime;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _fingerDownPosition = _cam.ScreenToViewportPoint(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            _fingerUpPosition = _cam.ScreenToViewportPoint(Input.mousePosition);
            DetectSwipe();
        }
        if (Input.GetMouseButtonUp(0))
        {
            _oneSwipeAtTime = true;
        }
    }

    private void LateUpdate()
    {
        InputManager.SetAxisY(0);
    }

    protected abstract void DetectSwipe();

    protected bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }
    protected bool SwipeDistanceCheck()
    {
        return VerticalMovementDistance() > _minDistanceForSwipe || HorizontalMovementDistance() > _minDistanceForSwipe;
    }

    private float VerticalMovementDistance()
    {
        return Mathf.Abs(_fingerDownPosition.y - _fingerUpPosition.y);
    }
    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(_fingerDownPosition.x - _fingerUpPosition.x);
    }

    protected void SendSwipe(SwipeDirection direction)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = direction,
            StartPosition = _fingerDownPosition,
            EndPosition = _fingerUpPosition,
            AdditionalDirection = null

        };
        OnSwipe(swipeData);
    }
    protected void SendSwipe(SwipeData swipeData)
    {
        OnSwipe(swipeData);
    }

    public void ChangeSwipeDistance(string valueDistance)
    {
        _minDistanceForSwipe = (float)Convert.ToDouble(valueDistance);
    }

}

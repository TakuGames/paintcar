﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SwipeData
{
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection Direction;
    public SwipeDirection? AdditionalDirection;
}

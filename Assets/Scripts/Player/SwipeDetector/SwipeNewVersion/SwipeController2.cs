﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController2 : MonoBehaviour
{
    private void OnEnable()
    {
        ASwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
    }
    private void OnDisable()
    {
        ASwipeDetector.OnSwipe -= SwipeDetector_OnSwipe;
    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        SetAxis(data.Direction);

        if (data.AdditionalDirection != null)
            SetAxis((SwipeDirection)data.AdditionalDirection);
        
    }

    private void SetAxis(SwipeDirection direction)
    {
        switch (direction)
        {
            case SwipeDirection.Up:
                InputManager.SetAxisY(1);
                break;
            case SwipeDirection.Down:
                InputManager.SetAxisY(-1);
                break;
            case SwipeDirection.Left:
                InputManager.SetAxisX(-1);
                break;
            case SwipeDirection.Right:
                InputManager.SetAxisX(1);
                break;
            default:
                break;
        }
    }
}

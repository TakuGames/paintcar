﻿using System;
using UnityEngine;

public class SwipeDetector2 : ASwipeDetector
{
    [Range(0, 45)]
    [SerializeField] private float _diagonalAngle = 22.5f;

    public float DiagonalAngle { get => _diagonalAngle; set => _diagonalAngle = value; }

    protected override void DetectSwipe()
    {
        if (SwipeDistanceCheck())
        {
            if (!_oneSwipeAtTime)
                return;
            SwipeDirection direction;
            SwipeDirection? additionalDirection = null;
            var swipeVector = _fingerUpPosition - _fingerDownPosition;
            if (IsVerticalSwipe())
            {
                direction = swipeVector.y < 0 ? SwipeDirection.Down : SwipeDirection.Up;
                var direct = swipeVector.y < 0 ? Vector2.down : Vector2.up;
                additionalDirection = DetectDiagonalDiretion(swipeVector, direct, direction, swipeVector.x, -direct.y);

            }
            else
            {
                direction = swipeVector.x < 0 ? SwipeDirection.Left : SwipeDirection.Right;
                var direct = swipeVector.x < 0 ? Vector2.left : Vector2.right;
                additionalDirection = DetectDiagonalDiretion(swipeVector, direct, direction, swipeVector.y, direct.x);
            }
            SendSwipe(direction, additionalDirection);

            _fingerUpPosition = _fingerDownPosition;

            if (_oneSwipeAtTime)
                _oneSwipeAtTime = false;
        }
    }

    private SwipeDirection? DetectDiagonalDiretion(Vector2 swipeVector, Vector2 direct, SwipeDirection direction, float axisParametr, float axisDirect)
    {
        SwipeDirection? additionalDirection = null;
        if (Vector2.Angle(direct, swipeVector) > DiagonalAngle)
            additionalDirection = (axisParametr * axisDirect) < 0 ? direction.Next() : direction.Prev();
        return additionalDirection;
    }

    private void SendSwipe(SwipeDirection direction, SwipeDirection? additionalDirection)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = direction,
            StartPosition = _fingerDownPosition,
            EndPosition = _fingerUpPosition,
            AdditionalDirection = additionalDirection
        };
        SendSwipe(swipeData);
    }
}




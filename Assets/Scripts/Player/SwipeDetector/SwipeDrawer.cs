﻿using UnityEngine;

public class SwipeDrawer : MonoBehaviour
{
    private void Awake()
    {
        ASwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        Vector2 startVector = new Vector2(data.StartPosition.x, data.StartPosition.y);
        Vector2 endVector = new Vector2(data.EndPosition.x, data.EndPosition.y);
        Debug.DrawLine(startVector, endVector, Color.black, 2);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    private void OnEnable()
    {
        SwipeDetector2.OnSwipe += SwipeDetector_OnSwipe;
    }
    private void OnDisable()
    {
        SwipeDetector2.OnSwipe -= SwipeDetector_OnSwipe;
    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        switch (data.Direction)
        {
            case SwipeDirection.Up:
                InputManager.SetAxisY(1);
                break;
            case SwipeDirection.Down:
                InputManager.SetAxisY(-1);
                break;
            case SwipeDirection.Left:
                InputManager.SetAxisX(-1);
                break;
            case SwipeDirection.Right:
                InputManager.SetAxisX(1);
                break;
            default:
                break;
        }
    }
}

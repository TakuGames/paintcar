﻿using UnityEngine;

public class Shadow : MonoBehaviour
{
    [SerializeField] private LayerMask _mask = 0;
    [SerializeField] private SpriteRenderer _shadow = null;

    private Collider2D _collider2D = null;

    private void Awake()
    {
        _collider2D = GetComponent<Collider2D>();
    }
    private void Update()
    {
        var hit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, _mask);

        if (hit)
        {
            var shadowPos = _shadow.transform.position;
            shadowPos.y = hit.point.y;

            _shadow.transform.position = shadowPos;

            var distance = (transform.position.y - _collider2D.bounds.size.y / 2) - hit.point.y;

            var shadowScale = _shadow.transform.localScale;
            shadowScale.x = 1 / distance;
            shadowScale.x = Mathf.Clamp(shadowScale.x, 0, 1);

            _shadow.transform.localScale = shadowScale;
        }
    }
}
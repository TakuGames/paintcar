﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBPlayerTurn : StateMachineBehaviour
{
    private SpriteRenderer _baseSR;
    private Transform _player;
    private PlayerConfig _config;

    private void Awake()
    {
        _baseSR = Reference.instance.player.baseSR;
        _player = Reference.instance.player.transform;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_player.rotation.y == 0)
        {
            _player.rotation = Quaternion.Euler(0,180,0);
            //_baseSR.flipY = true;
        }
        else
        {
            _player.rotation = Quaternion.Euler(0,0,0);
            //_baseSR.flipY = false;
        }
    }
}

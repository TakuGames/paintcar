﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASMBPlayer : StateMachineBehaviour
{
    protected PlayerConfig _config;
    protected Animator _animator;
    
    protected readonly int _jumpParam = Animator.StringToHash("Jump");
    protected readonly int _turnLeftParam = Animator.StringToHash("TurnLeft");
    protected readonly int _turnRightParam = Animator.StringToHash("TurnRight");

    private void Awake()
    {
        _config = Reference.instance.player.config;
        _animator = Reference.instance.player.animator;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBPlayer : StateMachineBehaviour
{
    private PlayerConfig _config;
    private Animator _animator;
    
    private readonly int _turnParam = Animator.StringToHash("Turn");
    private readonly int _jumpParam = Animator.StringToHash("Jump");
    private readonly int _downParam = Animator.StringToHash("Down");
    
    private EDirection _curDir;

    private void Awake()
    {
        _config = Reference.instance.player.config;
        _animator = Reference.instance.player.animator;
    }

    private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        _config.input.isInput = true;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        Turn();
        Jump();
        Down();
    }

    private void Turn()
    {
        if (_curDir == _config.move.direction)
            return;

        _curDir = _config.move.direction;
        _animator.SetTrigger(_turnParam);
    }
    
    private void Jump()
    {
        if (!_config.jump.isJump)
            return;
        
        _animator.SetTrigger(_jumpParam);
        _config.jump.isJump = false;
    }
    
    private void Down()
    {
        if (!_config.jump.isDown)
            return;
        
        _animator.SetTrigger(_downParam);
        _config.jump.isDown = false;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _config.input.isInput = false;
    }
}

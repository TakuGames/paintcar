﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBWheels : StateMachineBehaviour
{
    private PlayerConfig _playerConfig;

    private void Awake()
    {
        _playerConfig = Reference.instance.player.config;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("Move", _playerConfig.move.curSpeed > 0);
    }
}

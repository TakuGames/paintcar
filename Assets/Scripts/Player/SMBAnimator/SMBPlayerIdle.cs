﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBPlayerIdle : ASMBPlayer
{
    private readonly int _turnParam = Animator.StringToHash("Turn");
    
    private EDirection _curDir;

    private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        _curDir = _config.move.direction;
        _config.input.isInput = true;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        Rotate();
        Jump();
    }

    private void Rotate()
    {
        if (_curDir == _config.move.direction)
            return;
        
        _animator.SetTrigger(_turnParam);
    }

    private void Jump()
    {
        if (_config.jump.isJump)
        {
            _animator.SetTrigger(_jumpParam);
            _config.jump.isJump = false;
        }
    }


    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _config.input.isInput = false;
    }
}

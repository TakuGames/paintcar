﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBPlayerJump : ASMBPlayer
{
    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        _animator.SetBool(_jumpParam, _config.jump.isJump);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBPaint : StateMachineBehaviour
{
    private PaintController _paintController;
    private PlayerConfig _playerConfig;

    private void Awake()
    {
        _paintController = FindObjectOfType<PaintController>();
        _playerConfig = Reference.instance.player.config;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("Paint", _paintController.isPanting);
        
        animator.SetBool("Move", _playerConfig.move.curSpeed > 0);
    }
}

﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.PlayerLoop;
using Random = System.Random;

public class DirectionEvent : UnityEvent<EDirection>
{
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerConfig _config;
    [SerializeField] private Transform _filler;
    [SerializeField] private Transform _targetCamera;
    [SerializeField] private SpriteRenderer _baseSR;

    public PlayerConfig config => _config;
    public Transform filler => _filler;
    public Transform targetCamera => _targetCamera;
    public SpriteRenderer baseSR => _baseSR;

    private Rigidbody2D _rb;
    private Collider2D _collider2D;
    //private float _timeSinceGrounded;
    private Animator _animator;

    private Vector3 _posTargetCamRight;
    private Vector3 _posTargetCamLeft;
    private Vector2? _input;

    private ContactFilter2D _filter;
    private List<ContactPoint2D> _contacts = new List<ContactPoint2D>();

    public Animator animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();

            return _animator;
        }
    }


    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();
    }

    private void Start()
    {
        _filter = new ContactFilter2D();
        _filter.useLayerMask = true;
        _filter.layerMask = 1 << LayerMask.NameToLayer("Platform");
    }

    private void Update()
    {
        if (Math.Abs(_rb.velocity.y) > _config.jump.velocityLimit)
        {
            var velocity = _rb.velocity;
            velocity.y = Mathf.Clamp(velocity.y, -_config.jump.velocityLimit, _config.jump.velocityLimit);
            _rb.velocity = velocity;
        }
        
        CheckGround();

        Input();

        Rotate();
        Move();
        Jump();
        Down();
    }

    private void Input()
    {
        if (!IsPlatformed())
        {
            _input = Vector2.zero;
            return;
        }

        if (!_config.input.isInput)
            return;

        _input = _config.input.input;
    }
    
    
    private void Move()
    {
        float curPositionX = transform.position.x;

        if (_config.move.accelerationEnable)
        {
            _config.move.curSpeed = Mathf.Lerp(_config.move.curSpeed, _config.move.maxSpeed,
                _config.move.acceleration * Time.deltaTime);
        }
        else
        {
            _config.move.curSpeed = _config.move.maxSpeed;
        }

        transform.Translate(new Vector3(_config.move.curSpeed, 0f, 0f) * Time.deltaTime);
        _config.move.distancePerFrame = transform.position.x - curPositionX;
    }

    private void Rotate()
    {
        if (_input == Vector2.zero || _input.Value.x == 0)
            return;
        
        if (!IsPlatformed() || _config.move.blockDirection)
            return;

        EDirection curDirection = EDirection.Non;

        if (_input.Value.x > 0)
        {
            curDirection = EDirection.Right;
        }
        
        if (_input.Value.x < 0)
        {
            curDirection = EDirection.Left;
        }
        
        if (curDirection == _config.move.direction)
            return;

        _config.move.direction = curDirection;
        _config.move.curSpeed = 0;
    }


    private void Jump()
    {
        if (_input == Vector2.zero)
            return;
        
        if (_input.Value.y == 1)
        {
            _config.jump.isJump = true;
            _rb.AddForce(new Vector2(0f, _config.jump.jumpForce), ForceMode2D.Impulse);
        }
    }

    private void Down()
    {
        if (_input == Vector2.zero)
            return;
        
        if (_input.Value.y == -1 && IsPlatformed(true))
        {
            _config.jump.isDown = true;
            _collider2D.isTrigger = true;
        }
    }

    private bool IsPlatformed(bool checkGround = false)
    {
        _rb.GetContacts(_filter, _contacts);

        if (_contacts.Count == 0)
            return false;

        if (checkGround)
        {
            foreach (var contact in _contacts)
            {
                if (contact.collider.CompareTag("Ground"))
                    return false;
            }
        }

        return true;
    }

    private void CheckGround()
    {
        if (!_collider2D.isTrigger)
            return;

        var y = _collider2D.bounds.max.y;
        var x = _collider2D.bounds.center.x;

        var hit = Physics2D.Raycast(new Vector2(x, y), Vector2.down, _collider2D.bounds.size.y + 0.2f,
            _config.jump.mask);

        if (hit.collider == null)
        {
            _collider2D.isTrigger = false;
        }
    }
}
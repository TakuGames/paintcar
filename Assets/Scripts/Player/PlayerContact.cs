﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContact : AContact
{
    private CheckStuck _checkStuck;

    private void Awake()
    {
        base.Awake();
        _checkStuck = GetComponent<CheckStuck>();
    }

    protected override void Contact(AContact other)
    {
        _checkStuck.Check();
    }
}

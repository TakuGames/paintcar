﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class PlayerHealth : MonoBehaviour, IDamage
{
    private WaitForSeconds _waitDamageEffect;
    private WaitForSeconds _delayBlinking;
    private PlayerConfig _config;
    private SpriteRenderer[] _sprites;
    private readonly int _isFill = Shader.PropertyToID("_IsFill");

    private void Awake()
    {
        _sprites = GetComponentsInChildren<SpriteRenderer>();
        _config = Reference.instance.player.config;
    }

    void Start()
    {
        _config.health.curHealth = _config.health.startHealth;
        _waitDamageEffect = new WaitForSeconds(_config.health.timeDamageEffect);
        _delayBlinking = new WaitForSeconds(_config.health.delayBlinking);


        //_sprites[0].material.SetInt(_isFill, 1);
    }

    public void ChangeHealth(int lifes)
    {
        _config.health.curHealth += lifes;
    }

    public void Damage()
    {
        Debug.Log("Damage");

        Reference.instance.effectsController.UseEffectOnce(EFX.playerDamage, transform);

        ChangeHealth(-1);

        if (_config.health.curHealth <= 0)
        {
            StateGame.OnEndGame.Invoke();
        }
        else
        {
            StartCoroutine(DamageEffect());
        }
    }

    private IEnumerator DamageEffect()
    {
        gameObject.layer = LayerMask.NameToLayer("Immortal");
        var blinking = StartCoroutine(Blinking());

        yield return _waitDamageEffect;

        StopCoroutine(blinking);

        foreach (var sprite in _sprites)
        {
            sprite.material.SetInt(_isFill, 0);
        }

        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    private IEnumerator Blinking()
    {
        while (true)
        {
            foreach (var sprite in _sprites)
            {
                sprite.material.SetInt(_isFill, 1);
            }

            yield return _delayBlinking;

            foreach (var sprite in _sprites)
            {
                sprite.material.SetInt(_isFill, 0);
            }

            yield return _delayBlinking;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct Effect
{
    public string name;
    public EFX type;
    public ParticleSystem particleSystem;
}

public class VFXController : MonoBehaviour
{
    [SerializeField] private Effect[] _effects;

    IEnumerator StopingLerped(ParticleSystem ps, float speed)
    {
        if (speed == 0)
            yield break;

        WaitForEndOfFrame oneFrame = new WaitForEndOfFrame();

        while (ps.startLifetime > 0.01f)
        {
            ps.startLifetime -= speed * Time.deltaTime;
            yield return oneFrame;
        }

        Debug.Log("StopLerp effect");
        ps.Stop();
    }

    public void UseEffectOnce(EFX typeOfEffect, Transform effectTransform = null)
    {
        for (int i = 0; i < _effects.Length; i++)
        {
            if (_effects[i].type == typeOfEffect)
            {
                if (effectTransform)
                    _effects[i].particleSystem.transform.position = effectTransform.position;

                if (!_effects[i].particleSystem.gameObject.activeSelf)
                    _effects[i].particleSystem.gameObject.SetActive(true);

                _effects[i].particleSystem.Play();
            }
        }
    }
    public void StopEffectLerped(EFX typeOfEffect, float duration = 1)
    {
        for (int i = 0; i < _effects.Length; i++)
        {
            if (_effects[i].type == typeOfEffect)
            {
                StartCoroutine(StopingLerped(_effects[i].particleSystem, duration));
            }
        }
    }
}

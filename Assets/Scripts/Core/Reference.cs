﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reference : MonoBehaviour
{
    private static Reference _istance;
    public static Reference instance
    {
        get
        {
            if(_istance == null)
                _istance = (Reference) FindObjectOfType(typeof(Reference));
 
            return _istance;
        }
    }
    
    [Header("Global Reference")]
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private LevelController _levelController;
    [SerializeField] private ScoreController _scoreController;
    [SerializeField] private UIController _uiController;
    [SerializeField] private VFXController _effectsController;

    [Header("Core Reference")]
    [SerializeField] private SOLevelsData _levelsData;
    [SerializeField] private StateGame _stateGame;
    private StartChunk _startChunk;

    [Header("UI Reference")]
    [SerializeField] private GameObject _startBtn;

    public PlayerController player
    {
        get
        {
            if (_playerController == null)
                _playerController = FindObjectOfType<PlayerController>();

            return _playerController;
        }
    }
    public VFXController effectsController => _effectsController;
    public LevelController levelController => _levelController;
    public ScoreController score => _scoreController;
    public UIController ui => _uiController;
    public StateGame stateGame => _stateGame;
    public SOLevelsData levelsData => _levelsData;
   
    public StartChunk startChunk
    {
        get => _startChunk;
        set => _startChunk = value;
    }

    public GameObject startBtn => _startBtn;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager 
{
    private static float _xValue = 0;
    private static float _yValue = 0;
    

    public static float GetAxisX()
    {
        return _xValue;
    }
    public static float GetAxisY()
    {
        return _yValue;
    }

    public static void SetAxisX(float xValue)
    {
        _xValue = xValue;
    }
    public static void SetAxisY(float yValue)
    {
        _yValue = yValue;
    }
}

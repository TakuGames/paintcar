﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

[Serializable]
public class OnStarsEvent : UnityEvent<int> { }

public class ScoreController : MonoBehaviour
{
    public List<Colorable> CorableSprites => _spritesInLevel;

    [Header("UI reference")]
    [SerializeField] private UIScore _scorePaintWidget;
    [SerializeField] private ScoreData _scoreData;

    public ScoreData scoreData => _scoreData;

    private int _prevNumber = 0;
    private float _roundScore = 0;

    private List<Colorable> _spritesInLevel = new List<Colorable>();

    private void Start()
    {
        _scoreData.stars = 0;
        _scoreData.score = 0;
        _scoreData.deadEnemies = 0;

        foreach (var chunk in Reference.instance.levelController.chunks)
        {
            var items = chunk.GetComponentsInChildren<Colorable>(true);
            foreach (var item in items)
            {
                _spritesInLevel.Add(item);
            }
        }

        foreach (var item in _spritesInLevel)
        {
            item.OnGetFill += Calculate;
        }
    }

    private void OnDisable()
    {
        Unsubscribing();
    }

    private void Calculate(float spriteFill)
    {
        _scoreData.score += spriteFill / _spritesInLevel.Count;
        _roundScore = (float)Math.Round(_scoreData.score, 2, MidpointRounding.AwayFromZero);

        _scorePaintWidget.ShowScorePaint(_roundScore);

        if (_scoreData.stars < 1 && _roundScore >= 0.6 && _roundScore < 0.8f)
        {
            _scoreData.stars = 1;
            Reference.instance.effectsController.StopEffectLerped(EFX.rain);
        }

        if (_scoreData.stars < 2 && _roundScore >= 0.8f && _roundScore < 1.0f)
        {
            _scoreData.stars = 2;
        }

        if (_scoreData.stars < 3 && _roundScore >= 1.0f)
        {
            _scoreData.stars = 3;
            Unsubscribing();
            StateGame.OnEndGame.Invoke();
        }
    }
    
    private void CountingEnemies()
    {
        scoreData.deadEnemies++;
    }
    
    private void Unsubscribing()
    {
        foreach (var item in _spritesInLevel)
        {
            item.OnGetFill -= Calculate;
        }
    }
}


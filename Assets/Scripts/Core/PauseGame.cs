﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    private static int counter = 0;

    public static void Pause()
    {
        Time.timeScale = (counter % 2 == 0) ? 0 : 1;
        counter++;
    }
    public static void Pause(bool inPause)
    {
        Time.timeScale = (inPause) ? 0 : 1;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class StateGame : MonoBehaviour
{
    [NonSerialized] public static readonly UnityEvent OnLoadLevel = new UnityEvent();
    [NonSerialized] public static readonly UnityEvent OnLoadPlayer = new UnityEvent();
    [NonSerialized] public static readonly UnityEvent OnStartGame = new UnityEvent();
    [NonSerialized] public static readonly UnityEvent OnPlayGame = new UnityEvent();
    [NonSerialized] public static readonly UnityEvent OnPauseGame = new UnityEvent();
    [NonSerialized] public static readonly UnityEvent OnEndGame = new UnityEvent();


    IEnumerator EndingGame()
    {
        WaitForSeconds seconds = new WaitForSeconds(0.5f);
        yield return seconds;
        Time.timeScale = 0;
    }
    private void Start()
    {
        Application.targetFrameRate = 60;

        PlayGame();

        OnPlayGame.AddListener(PlayGame);
        OnStartGame.AddListener(StopGame);
        OnPauseGame.AddListener(StopGame);
        OnEndGame.AddListener(EndGame);
    }


    private void EndGame()
    {
        StartCoroutine(EndingGame());
    }

    private static void StopGame()
    {
       // Time.timeScale = 0;
    }
    public static void PlayGame()
    {
        Time.timeScale = 1;
    }
    public static void RestartGame()
    {
        Reference.instance.levelsData.loaded.selected = true;
        SceneManager.LoadScene(0);
        Debug.Log($"cur level {Reference.instance.levelsData.loaded.level}");
    }
    public static void NextLevel()
    {
        Reference.instance.levelsData.loaded.selected = false;
        SceneManager.LoadScene(0);
        Debug.Log($"cur level {Reference.instance.levelsData.loaded.level}");
    }
    public static void LoadLevel(int location, int level)
    {
        Reference.instance.levelsData.loaded.selected = true;
        Reference.instance.levelsData.loaded.location = location;
        Reference.instance.levelsData.loaded.level = level;
    }
    public static void ExitGame()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
        OnPlayGame.RemoveListener(PlayGame);
        OnStartGame.RemoveListener(StopGame);
        OnPauseGame.RemoveListener(StopGame);
        OnEndGame.RemoveListener(EndGame);
    }
}

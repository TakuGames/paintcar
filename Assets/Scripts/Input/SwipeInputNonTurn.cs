﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class SwipeInputNonTurn : MonoBehaviour
{
    [SerializeField] private float _distanceUnitDetectSwipe;
    [SerializeField] private float _angleAxis = 15f;

    private float _distanceDetect;
    private float _angleNorm;
    private Vector2 _downInputPos;
    private Vector2 _curInputPos;
    private bool _isSwipe;
    private Camera _cam;
    private Vector2 _input;
    private PlayerConfig _playerConfig;

    private void Awake()
    {
        _cam = Camera.main;
        _playerConfig = Reference.instance.player.config;
    }

    private void Start()
    {
        _distanceDetect = (_cam.pixelHeight / (2 * _cam.orthographicSize) * _distanceUnitDetectSwipe);
        _angleNorm = _angleAxis / 90f;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isSwipe = true;
            _downInputPos = Input.mousePosition;
        }

        if (!_isSwipe)
            return;

        if (Input.GetMouseButton(0))
        {
            _curInputPos = Input.mousePosition;
            var distance = (_curInputPos - _downInputPos).magnitude;

            if (distance <= _distanceDetect)
                return;

            _isSwipe = false;
            
            CheckAxis();
        }
    }

    private void CheckAxis()
    {
        _input = Vector2.zero;
        Vector2 swipe = (_curInputPos - _downInputPos).normalized;
        var angleDot = Math.Abs(Vector2.Dot(swipe, Vector2.up));

        if (angleDot >= _angleNorm)
        {
            _input = _curInputPos.y > _downInputPos.y ? Vector2.up : Vector2.down;
        }

        _playerConfig.input.input = _input;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SOLevelsData))]
public class SOLevelsDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        SOLevelsData myScript = (SOLevelsData)target;
        if(GUILayout.Button("Reset Progress"))
        {
            myScript.ResetProgress();
        }
        
        if(GUILayout.Button("Print Stars Count"))
        {
            myScript.GetStarsCount();
        }
        
                
        if(GUILayout.Button("Generate UI"))
        {
            var go = GameObject.Find("StartWindow");
            if (!go) return;
            
            go.GetComponent<ChooseLevelWindow>().Place();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Data/LevelsData", fileName = "NewLevelsData")]
public class SOLevelsData : ScriptableObject
{
    
    [Serializable]
    public class Location
    {
        public string name;
        public bool available;
        public int starsForOpen;
        public GameObject backgroundPrefab;
        public GameObject arrivalChunkPrefab;
        public GameObject startChunkPrefab;
        public Sprite backgroundLoadLevel;
        
        public LevelData[] levels;
        [HideInInspector] public bool _prof = false;
    }

    public class Loaded
    {
        public bool selected = false;
        public int location = 0;
        public int level = 0;
    }

    [SerializeField] private Location[] _locations;
    private int _starsCount;
    private Loaded _loaded;


    public int locationsCount => _locations.Length;

    public Loaded loaded
    {
        get => _loaded;
        set => _loaded = value;
    }

    public Location GetLocation(int index)
    {
        return _locations[index];
    }

    public Loaded SelectLevel()
    {
        if (_loaded == null)
            _loaded = new Loaded();

        if (_loaded.selected)
        {
            _loaded.selected = false;
            return _loaded;
        }


        _starsCount = GetStarsCount();

        int locationIndex = 0;
        int? levelIndex = null;

        LocationIndex(ref locationIndex);
        LevelIndex(_locations[locationIndex], ref levelIndex);

        _loaded.selected = false;
        _loaded.location = locationIndex;
        _loaded.level = (int) levelIndex;

        return _loaded;
    }

    private void LocationIndex(ref int locationIndex)
    {
        for (int i = 0; i < _locations.Length; i++)
        {
            if (_locations[i].available)
            {
                locationIndex = i;
            }
            else
            {
                if (_locations[i].starsForOpen > _starsCount)
                    break;

                var lastLocation = _locations[i - 1];
                var lastLevel = lastLocation.levels[lastLocation.levels.Length - 1];

                if (lastLevel.stars <= 0)
                    break;

                _locations[i].available = true;
                locationIndex = i;
            }
        }
    }

    private void LevelIndex(Location location, ref int? levelIndex)
    {
        for (int i = 0; i < location.levels.Length; i++)
        {
            if (location.levels[i].stars <= 0)
            {
                levelIndex = i;
                break;
            }
        }

        if (levelIndex == null)
            levelIndex = location.levels.Length - 1;
    }

    public int GetStarsCount()
    {
        int starsCount = 0;

        foreach (var location in _locations)
        {
            if (!location.available)
                break;

            foreach (var level in location.levels)
            {
                if (level.stars == 0)
                    break;

                starsCount += level.stars;
            }
        }

        Debug.Log($"All stars {starsCount}");
        return starsCount;
    }


    //reset all stars in all levels
    public void ResetProgress()
    {
        PlayerPrefs.SetInt("Location", 0);
        PlayerPrefs.SetInt("Level", 0);

        foreach (var location in _locations)
        {
            location.available = false;

            foreach (var level in location.levels)
            {
                level.stars = 0;
            }
        }

        _locations[0].available = true;

        Debug.Log(
            $"Reset Progress. Write start location {PlayerPrefs.GetInt("Location")}, level {PlayerPrefs.GetInt("Level")}");
    }
}
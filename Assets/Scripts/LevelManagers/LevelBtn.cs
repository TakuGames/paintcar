﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelBtn : MonoBehaviour
{    
    [SerializeField] private TextMeshProUGUI _numberLevel;
    [SerializeField] private Image[] _stars;
    [SerializeField] private GameObject _block;

    [SerializeField] private int _locationIndex;
    [SerializeField] private int _levelIndex;

    public void Write(int locationIndex, int levelIndex, int starsCount)
    {
        _locationIndex = locationIndex;
        _levelIndex = levelIndex;

        _numberLevel.text = $"{levelIndex + 1}";

        Block();
        Stars();
    }

    private void Start()
    {
        Block();
        Stars();
    }

    public void OnBtn()
    {
        SOLevelsData.Loaded loaded = new SOLevelsData.Loaded();
        loaded.selected = true;
        loaded.location = _locationIndex;
        loaded.level = _levelIndex;
        Reference.instance.levelsData.loaded = loaded;
        
        StateGame.RestartGame();
    }

    private void Block()
    {
        if (Reference.instance.levelsData.GetLocation(_locationIndex).levels[_levelIndex].stars > 0)
        {
            _block.SetActive(false);
            return;
        }

        if (_levelIndex == 0 && Reference.instance.levelsData.GetLocation(_locationIndex).available)
        {
            _block.SetActive(false);
            return;
        }

        if (_levelIndex > 0 && Reference.instance.levelsData.GetLocation(_locationIndex).levels[_levelIndex - 1].stars > 0)
        {
            _block.SetActive(false);
            return;
        }
        
        _block.SetActive(true);
    }

    private void Stars()
    {
        int stars = Reference.instance.levelsData.GetLocation(_locationIndex).levels[_levelIndex].stars;
        
        for (int i = 0; i < _stars.Length; i++)
        {
            if (i < stars)
            {
                _stars[i].color = Color.white;
            }
            else
            {
                _stars[i].color = Color.black;
            }
        }
    }
    
}

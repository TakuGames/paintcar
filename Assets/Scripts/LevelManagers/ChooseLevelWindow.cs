﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ChooseLevelWindow : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject _locationGroupPrefab;
    [SerializeField] private GameObject _levelBtnPrefab;

    [Header("Reference")]
    [SerializeField] private RectTransform _content;

    [ContextMenu("Place Buttons")]
    public void Place()
    {
        Clear();
        Debug.Log("Placed");
        var levelsData = Reference.instance.levelsData;
        var starsCount = levelsData.GetStarsCount();
        GameObject[] locationsGO = new GameObject[levelsData.locationsCount];

        for (int i = 0; i < levelsData.locationsCount; i++)
        {
            locationsGO[i] = Instantiate(_locationGroupPrefab, _content.transform);

            var location = levelsData.GetLocation(i);

            locationsGO[i].GetComponent<Image>().sprite = location.backgroundLoadLevel;

            location.available = starsCount >= location.starsForOpen;

            for (int j = 0; j < location.levels.Length; j++)
            {
                var btn = Instantiate(_levelBtnPrefab, locationsGO[i].transform);
                btn.GetComponent<LevelBtn>().Write(i, j, location.levels[j].stars);
            }
        }
    }

    [ContextMenu("Clear Buttons")]
    private void Clear()
    {
        Debug.Log("Clear");

        while (_content.childCount > 0)
        {
            DestroyImmediate(_content.GetChild(0).gameObject);
        }
    }
}

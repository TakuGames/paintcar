﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestShader : MonoBehaviour
{
    [SerializeField] private List<Section> _sections = new List<Section>();
    [SerializeField] private SetSectionsInShader _shader = null;

    private void Update()
    {
        if (_shader)
            _shader.SetSections = _sections;
    }
}

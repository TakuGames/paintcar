﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpritePendulum : MonoBehaviour
{
    private void Start()
    {
        transform.DOMoveX(1, 1).SetLoops(2, LoopType.Yoyo).OnComplete(Complite);
    }

    void Complite()
    {
        transform.DOMoveX(2, 1).SetLoops(2, LoopType.Yoyo).OnComplete(Complite);
    }
}

﻿using System;
using UnityEngine;
using TMPro;

public class RaycastController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _raysDistanceTxt;
    [SerializeField] private TextMeshProUGUI _countOfRaycstsTxt;

    private int _countOfRaycasts = 15;
    private bool _checkDistance = true;

    void Update()
    {
        float distance = 0;
        for (int i = 0; i < _countOfRaycasts; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, Mathf.Infinity);

            if (hit && _checkDistance)
                distance += hit.distance;
        }
        _raysDistanceTxt.text = (_checkDistance) ? "Raycasts distance: \n" + distance.ToString() : "";   
        _countOfRaycstsTxt.text = "Count of Raycasts: \n" + _countOfRaycasts.ToString();
    }

    public void CountOfRaycasts(string countOfRaycasts)
    {
        if (countOfRaycasts.Length > 0)
            _countOfRaycasts = Convert.ToInt32(countOfRaycasts);
    }
    public void CheckDistance(bool value)
    {
        _checkDistance = value;
    }
}
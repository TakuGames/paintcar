﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class SetSectionsInShader : MonoBehaviour
{
    private bool _flipX = false;

    private List<Section> _sections;

    private Vector4[] _sections4 = new Vector4[20];
    private Material _material;

    public List<Section> SetSections
    {
        set
        {
            _sections = value;
            UpdateShader();
        }
    }

    public Color SetColor { get; set; }

    private void Awake()
    {
        var colorable = GetComponent<Colorable>();
        if (colorable)
            _material = colorable.GetSprite.material;

        var spriteChildren = GetComponentInChildren<SpriteRenderer>();
        if (spriteChildren)
            _flipX = spriteChildren.flipX;

        if (_material)
            _material.SetInt("_FlipX", Convert.ToInt32(_flipX));
    }

    private void UpdateShader()
    {
        for (int i = 0; i < _sections.Count; i++)
        {
            _sections4[i].x = (_flipX) ? 1 - _sections[i].xMinNorm : _sections[i].xMinNorm;
            _sections4[i].y = (_flipX) ? 1 - _sections[i].xMaxNorm : _sections[i].xMaxNorm;
            _sections4[i].w = 0;
            _sections4[i].z = 0;
        }
        _material.SetInt("_SectionsLength", _sections.Count);
        _material.SetVectorArray("_Sections", _sections4);

        if (SetColor.a == 0)
            return;

        _material.SetColor("_Color", SetColor);
    }
}
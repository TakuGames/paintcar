﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float ytranslation = SimpleInput.GetAxis("Vertical");
        float xtranslation = SimpleInput.GetAxis("Horizontal");
        Debug.Log(ytranslation +" "+ xtranslation);
        transform.Translate(xtranslation , ytranslation , 0f);
    }
}

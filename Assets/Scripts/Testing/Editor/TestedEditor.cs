﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor((typeof(Tested)))]
public class TestedEditor : Editor
{
    private Sprite _curSprite;
    private SpriteRenderer _sr;

    private void OnEnable()
    {
        _sr = ((MonoBehaviour) this.target).gameObject.GetComponent<SpriteRenderer>();
        _curSprite = _sr.sprite;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        var sprite = (Sprite) serializedObject.FindProperty("_sprite").objectReferenceValue;
        if (_curSprite != sprite)
        {
            _curSprite = sprite;
            _sr.sprite = sprite;
            Debug.Log($"change {_curSprite.name}");
        }
    }
}

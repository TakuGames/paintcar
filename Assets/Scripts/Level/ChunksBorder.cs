﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct Border
{
    public float maxY;
    public float minY;
    public float maxX;
    public float minX;
}


public class ChunksBorder : MonoBehaviour
{
    private PolygonCollider2D _collider;
    private SizeCamera _camera;

    private Vector2 _leftTop;
    private Vector2 _leftBottom;
    private Vector2 _rightTop;
    private Vector2 _rightBottom;

    private float _top;
    private float _bottom;
    
    public float top
    {
        get => _top;
        set => _top = value;
    }

    public float bottom
    {
        get => _bottom;
        set => _bottom = value;
    }

    private void Awake()
    {
        _collider = GetComponent<PolygonCollider2D>();
        _camera = FindObjectOfType<SizeCamera>();
    }

    public void AssignBorder(float left, float right)
    {
        _leftTop = new Vector2(left, _top);
        _leftBottom = new Vector2(left, _bottom);
        _rightTop = new Vector2(right, _top);
        _rightBottom = new Vector2(right, _bottom);
        
        Vector2[] points = new Vector2[4];

        points[0] = _leftBottom;
        points[1] = _leftTop;
        points[2] = _rightTop;
        points[3] = _rightBottom;

        _collider.points = points;
        
        _camera.PathCache();
    }
}
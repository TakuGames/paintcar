﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class StarEventsController : MonoBehaviour
{
    private List<GameObject> _GOZeroStars = new List<GameObject>();
    private List<GameObject> _GOOneStar = new List<GameObject>();
    private List<GameObject> _GOTwoStars = new List<GameObject>();

    private ScoreData _scoreData;

    private void Awake()
    {
        _scoreData = Reference.instance.score.scoreData;
        _scoreData.OnStars.AddListener(Activate);
    }

    private void Start()
    {
        Generate();
        ActivateInCollection(_GOZeroStars);
    }

    private void Generate()
    {
        List<TaggedObject> objects = new List<TaggedObject>();
        
        foreach (var chunk in Reference.instance.levelController.chunks)
        {
            var items = chunk.GetComponentsInChildren<TaggedObject>(true);
            foreach (var item in items)
            {
                objects.Add(item);
            }
        }

        foreach (var obj in objects)
        {
            switch (obj.tag)
            {
                case ETag.ZeroStars:
                    _GOZeroStars.Add(obj.gameObject);
                    break;
                case ETag.OneStar:
                    _GOOneStar.Add(obj.gameObject);
                    break;
                case ETag.TwoStars:
                    _GOTwoStars.Add(obj.gameObject);
                    break;
            }

            obj.gameObject.SetActive(false);
            Destroy(obj);
        }
    }

    private void Activate()
    {
        switch (_scoreData.stars)
        {
            case 1:
                ActivateInCollection(_GOOneStar);
                break;
            case 2:
                ActivateInCollection(_GOTwoStars);
                break;
            default:
                break;
        }
    }

    private void ActivateInCollection(List<GameObject> collection)
    {
        foreach (var obj in collection)
        {
            obj.SetActive(true);
            obj.GetComponent<IActivate>()?.Activate();
        }
    }

    private void OnDestroy()
    {
        StateGame.OnLoadLevel.RemoveListener(Generate);
        _scoreData.OnStars.RemoveListener(Activate);
    }
}

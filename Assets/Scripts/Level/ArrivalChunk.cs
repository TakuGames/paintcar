﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalChunk : AChunk
{
    [SerializeField] private float _startOffsetCamera = 3f;
    [SerializeField] private Transform _startPoint;

    private bool _isDestroy = false;

    public bool isDestroy
    {
        set => _isDestroy = value;
    }
    
    public float startOffsetCamera => _startOffsetCamera;
    public Transform startPoint => _startPoint;
    
    public int index;
    public bool last;
    
    public IndexChunkEvent OnChunkExit = new IndexChunkEvent();
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.GetComponent<PlayerController>())
        return;

        if (!_isDestroy)
        {
            OnChunkExit.Invoke(index);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}

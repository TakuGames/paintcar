﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalChunkSwitcher : MonoBehaviour
{
    private ArrivalController _controller;
    private ChunksBorder _chunksBorder;
    private int _lastIndex;

    private void Awake()
    {
        _controller = GetComponent<ArrivalController>();
        _chunksBorder = GetComponent<ChunksBorder>();
        StateGame.OnPlayGame.AddListener(Disable);
    }

    private void Start()
    {
        _lastIndex = _controller.chunksCount - 1;
        
        for (int i = 0; i < _controller.chunksCount; i++)
        {
            var chunk = _controller.GetChunk(i);
            chunk.index = i;
            chunk.OnChunkExit.AddListener(Change);
        }

        _chunksBorder.AssignBorder(_controller.GetChunk(0).border.minX + _controller.GetChunk(0).startOffsetCamera, _controller.GetChunk(_lastIndex).border.maxX);
    }

    private void Change(int index)
    {
        var moveChunk = _controller.GetChunk(0);
        var newPosition = _controller.GetChunk(_lastIndex).transform.position;
        newPosition.x += moveChunk.width;
        moveChunk.transform.position = newPosition;

        for (int i = 0; i < _lastIndex; i++)
        {
            _controller.SetChunk(i, _controller.GetChunk(i + 1));
            _controller.GetChunk(i).index = i;
        }

        moveChunk.index = _lastIndex;
        _controller.SetChunk(_lastIndex, moveChunk);
        _chunksBorder.AssignBorder(_controller.GetChunk(0).border.minX, _controller.GetChunk(_lastIndex).border.maxX);
    }

    private void Disable()
    {
        for (int i = 0; i < _controller.chunksCount; i++)
        {
            var chunk = _controller.GetChunk(i);
            chunk.OnChunkExit.RemoveListener(Change);
            chunk.isDestroy = true;
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < _controller.chunksCount; i++)
        {
            var chunk = _controller.GetChunk(i);
            chunk.OnChunkExit.RemoveListener(Change);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private Transform _target;
    private ParallaxElement[] _parallaxElements;
    
    private void Start()
    {
        if (Camera.main != null) _target = Camera.main.transform;

        PlaceBackground();

        _parallaxElements = GetComponentsInChildren<ParallaxElement>();      
    }

    private void PlaceBackground()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        var loaded = Reference.instance.levelsData.loaded;
        Instantiate(Reference.instance.levelsData.GetLocation(loaded.location).backgroundPrefab, gameObject.transform);
    }
    private void LateUpdate()
    {
        if (!_target) return;

        foreach (var element in _parallaxElements)
            element.Parallaxing(_target);
    }
}

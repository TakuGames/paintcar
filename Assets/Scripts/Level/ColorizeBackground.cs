﻿using System;
using System.Collections;
using UnityEngine;

public class ColorizeBackground : MonoBehaviour
{
    [SerializeField] private float _percentEffect;

    public bool isColor
    {
        get => _isColor;
        set => _isColor = value;
    }

    private ScoreData _scoreData;
    private Material _material;

    private float _currentScore;

    private float _startBrightness;
    private float _startContrast;
    private float _startRedColor;
    private float _startGreenColor;

    private bool _isColor = false;

    private void Awake()
    {
        var spRenderer = GetComponentInChildren<SpriteRenderer>();
        _scoreData = Reference.instance.score.scoreData;
        _material = spRenderer.material;
        
        var collider = GetComponent<Collider2D>();
        if (collider != null)
            DestroyImmediate(collider);
    }
    private void OnEnable()
    {
        _isColor = false;

        _startBrightness = _material.GetFloat("_Brightness");
        _startContrast = _material.GetFloat("_Contrast");
        _startRedColor = _material.GetFloat("_RedColor");
        _startGreenColor = _material.GetFloat("_GreenColor");
    }

    public void Colorization(bool choose)
    {
        int number = (choose) ? 1 : 0;
        _material.SetInt("_IsShowColor", number);
    }
    public void ColoringBackground()
    {
        if (!_isColor)
            return;

        if (_scoreData.score > _percentEffect)
            return;

        float currentScore = _scoreData.score * (1 / _percentEffect);

        if (_currentScore != currentScore)
        {
            _material.SetFloat("_Saturation", currentScore);

            SettingValue("_Brightness", _startBrightness - (_startBrightness * currentScore));
            SettingValue("_RedColor", _startRedColor - (_startRedColor * currentScore));
            SettingValue("_GreenColor", _startGreenColor - (_startGreenColor * currentScore));
            SettingValue("_Contrast", _startContrast + ((1 - _startContrast) * currentScore));

            _currentScore = currentScore;
        }
    }

    public void ColoringToMono(float lerpSaturation, float reverseSaturation)
    {
        SettingValue("_Brightness", _startBrightness * reverseSaturation);
        SettingValue("_RedColor", _startRedColor * reverseSaturation);
        SettingValue("_GreenColor", _startGreenColor * reverseSaturation);
        SettingValue("_Contrast", 1 - ((1 - _startContrast) * reverseSaturation));
        SettingValue("_Saturation", lerpSaturation);
    }
    private void SettingValue(string type, float value)
    {
        float roundedValue = (float)Math.Round(value, 3, MidpointRounding.AwayFromZero);
        _material.SetFloat(type, roundedValue);
    }
}

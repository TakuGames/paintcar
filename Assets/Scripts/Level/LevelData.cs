﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/LevelData", fileName = "NewLevelData")]
public class LevelData : ScriptableObject
{
    public ECity city;
    public GameObject startChunk;
    public GameObject[] levelChunks;

    public int stars;
}

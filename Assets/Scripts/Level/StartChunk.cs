﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.PlayerLoop;

public class StartChunk : AChunk
{
    private void Awake()
    {
        base.Awake();
        Reference.instance.startChunk = this;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player"))
            return;

        _chunkSwitcher.RemoveStartChunk();

        Reference.instance.player.config.move.blockDirection = false;
        Destroy(gameObject);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalController : MonoBehaviour
{
    private PlayerController _playerController;
    private List<ArrivalChunk> _chunks = new List<ArrivalChunk>();
    
    public int chunksCount => _chunks.Count;

    public ArrivalChunk GetChunk(int index)
    {
        return _chunks[index];
    }

    public void SetChunk(int index, ArrivalChunk chunk)
    {
        _chunks[index] = chunk;
    }

    private void Awake()
    {
        _playerController = Reference.instance.player;
    }

    private void Start()
    {
        StartLevel();
        StartPlayer();
        StateGame.OnStartGame.Invoke();
        StateGame.OnPlayGame.AddListener((() => { _playerController.config.move.maxSpeed = 6;}));
    }

    private void StartLevel()
    {
        var loaded = Reference.instance.levelsData.loaded;
        var arrivalChunk = Reference.instance.levelsData.GetLocation(loaded.location).arrivalChunkPrefab;

        var position = Vector2.zero;

        for (int i = 0; i < 2; i++)
        {
            var chunk = Instantiate(arrivalChunk, position, Quaternion.identity, this.transform).GetComponent<ArrivalChunk>();
            position.x += chunk.width;
            _chunks.Add(chunk);
        }
    }

    private void StartPlayer()
    {
        _playerController.config.move.maxSpeed = 3;
        _playerController.config.move.direction = EDirection.Right;
        _playerController.config.move.blockDirection = true;
        _playerController.transform.position = _chunks[0].startPoint.position;
    }

    private void OnDestroy()
    {
        StateGame.OnPlayGame.RemoveListener((() => { _playerController.config.move.maxSpeed = 6;}));
    }
}

﻿using UnityEngine;

public class ParallaxElement : MonoBehaviour
{
    [SerializeField] private float _parallaxEffect;

    private float _spriteLenght;
    private float _startPosition;

    private void Awake()
    {
        var spRenderer = GetComponentInChildren<SpriteRenderer>();

        _spriteLenght = spRenderer.bounds.size.x;
        _startPosition = transform.position.x;

        var leftSprite = Instantiate(spRenderer);
        var rightSprite = Instantiate(spRenderer);

        leftSprite.transform.position = new Vector2(transform.position.x - _spriteLenght, spRenderer.transform.position.y);
        rightSprite.transform.position = new Vector2(transform.position.x + _spriteLenght, spRenderer.transform.position.y);

        leftSprite.transform.parent = transform;
        rightSprite.transform.parent = transform;

        leftSprite.name = "Sprite_(1)";
        rightSprite.name = "Sprite_(2)";
    }

    public void Parallaxing(Transform target)
    {
        float temp = target.position.x * (1 - _parallaxEffect);
        float dist = target.position.x * _parallaxEffect;

        transform.position = new Vector2(_startPosition + dist, transform.position.y);

        if (temp > _startPosition + _spriteLenght) _startPosition += _spriteLenght;
        else if (temp < _startPosition - _spriteLenght) _startPosition -= _spriteLenght;
    }
}

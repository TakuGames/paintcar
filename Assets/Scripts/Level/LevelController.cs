﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelController : MonoBehaviour
{
    [SerializeField] private bool _disableObjects = false;
    private Vector2 _position;
    private ChunksBorder _chunksBorder;
    private StartChunk _startChunk;
    private List<LevelChunk> _chunks;
    private LevelData _level;

    public StartChunk startChunk => _startChunk;
    public List<LevelChunk> chunks => _chunks;
    public ChunksBorder border => _chunksBorder;
    public LevelData levelData => _level;

    private void Awake()
    {
        _chunksBorder = GetComponent<ChunksBorder>();
        
        var loaded = Reference.instance.levelsData.loaded;
        if (loaded == null)
        {
            loaded = new SOLevelsData.Loaded();
            loaded.location = 0;
            loaded.level = 0;
            Reference.instance.levelsData.loaded = loaded;
        }
    }

    public void Start()
    {
        Clear();
        LoadLevelData();
        PlaceLevel();

        StateGame.OnLoadLevel.Invoke();
        StateGame.OnLoadPlayer.Invoke();
        StateGame.OnPlayGame.AddListener(EnableLevel);
        
        DisableObjects(); //for testing
    }

    private void Clear()
    {
        foreach (Transform child in gameObject.transform)
        {
            child.gameObject.SetActive(false);
            Destroy(child.gameObject);
        }
    }

    private void LoadLevelData()
    {
        var loaded = Reference.instance.levelsData.loaded;
        
        _level = Reference.instance.levelsData.GetLocation(loaded.location).levels[loaded.level];

        Debug.Log($"Load location {loaded.location + 1}, level {loaded.level + 1}");
    }

    private void PlaceLevel()
    {
        _startChunk = Instantiate(_level.startChunk, _position, Quaternion.identity, this.transform).GetComponent<StartChunk>();
        _startChunk.gameObject.SetActive(false);
        
        _chunks = new List<LevelChunk>();
        
        foreach (var chunkGO in _level.levelChunks)
        {
            _chunks.Add(Place(chunkGO));
        }
        
        border.top = _chunks[0].border.maxY;
        border.bottom = _chunks[0].border.minY;

        foreach (var chunk in _chunks)
        {
            chunk.gameObject.SetActive(false);
        }
    }
    
    public void EnableLevel()
    {
        var arrivalController = GetComponent<ArrivalController>();
        var lastChunk = arrivalController.GetChunk(arrivalController.chunksCount - 1);

        var position = lastChunk.transform.position;
        position.x += lastChunk.offset.x;
        position.x += lastChunk.width * 0.5f;
        
        _startChunk.gameObject.SetActive(true);
        position.x -= _startChunk.offset.x;
        position.x += _startChunk.width * 0.5f;
        _startChunk.transform.position = position;

        position.x += _startChunk.offset.x;
        position.x += _startChunk.width * 0.5f;
        
        for (int i = 0; i < _chunks.Count; i++)
        {
            _chunks[i].gameObject.SetActive(true);
            position.x -= _chunks[i].offset.x;
            position.x += _chunks[i].width * 0.5f;
            _chunks[i].transform.position = position;
            
            position.x += _chunks[i].offset.x;
            position.x += _chunks[i].width * 0.5f;
        }
        
        border.AssignBorder(arrivalController.GetChunk(0).border.minX, _chunks[_chunks.Count - 1].border.maxX);

        StateGame.OnPlayGame.RemoveListener(EnableLevel);
        
        Destroy(arrivalController);
        Destroy(GetComponent<ArrivalChunkSwitcher>());
    }
    

    private LevelChunk Place(GameObject chunkGO)
    {
        var chunk = Instantiate(chunkGO, _position, Quaternion.identity, this.transform).GetComponent<LevelChunk>();
//        chunk.transform.position = _position;
//        _position.x += chunk.width;
        return chunk;
    }

    private void DisableObjects()
    {
        if (_disableObjects)
        {
            foreach (Transform chunk in gameObject.transform)
            {
                foreach (Transform child in chunk)
                {
                    if (child.gameObject.layer != 8)
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }
    }
    
}

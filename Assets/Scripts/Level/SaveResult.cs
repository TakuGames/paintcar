﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveResult : MonoBehaviour
{
    private void Start()
    {
        StateGame.OnEndGame.AddListener(Save);
    }

    private void Save()
    {
        Debug.Log("Save");
        var stars = Reference.instance.score.scoreData.stars;
        
        if(stars == 0)
            return;

        Reference.instance.levelController.levelData.stars = stars;
    }

    private void OnDestroy()
    {
        StateGame.OnEndGame.RemoveListener(Save);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkSwitcher : MonoBehaviour
{
    private LevelController _controller;
    //private List<LevelChunk> _chunks;
    private int _last;
    private int _count;

    private void Awake()
    {
        _controller = GetComponent<LevelController>();
        StateGame.OnLoadLevel.AddListener(GenerateCollection);
    }

    public void GenerateCollection()
    {
        _last = _controller.chunks.Count - 1;
        _count = _controller.chunks.Count;
        
//        _controller.border.top = _controller.startChunk.border.maxY;
//        _controller.border.bottom = _controller.startChunk.border.minY;
        
        for (int i = 0; i < _count; i++)
        {
            var chunk = GetChunk(i);
            
            chunk.index = i;

            _controller.chunks[i].last = false;

            _controller.chunks[i].OnChunkEnter.AddListener(LeftChunkEnable);
            _controller.chunks[i].OnChunkExit.AddListener(RightChunkEnable);
        }

        _controller.chunks[_last].last = true;
        //_controller.border.AssignBorder(_controller.startChunk.border.minX + _controller.startChunk.startOffsetCamera, _controller.chunks[_last].border.maxX);
        
       StateGame.OnLoadLevel.RemoveListener(GenerateCollection);
    }
    

    private void LeftChunkEnable(int index)
    {
        //Left enable
        
        if (index > _count - 3)
            return;

        var moveChunk = _controller.chunks[_last];
        var newPosition = _controller.chunks[0].transform.position;
        newPosition.x -= moveChunk.width;
        moveChunk.transform.position = newPosition;

        for (int i = _last; i > 0; i--)
        {
            _controller.chunks[i] = _controller.chunks[i - 1];
            _controller.chunks[i].index = i;
            _controller.chunks[i].last = false;
        }

        moveChunk.index = 0;
        _controller.chunks[0] = moveChunk;
        _controller.chunks[_last].last = true;
        _controller.border.AssignBorder(_controller.chunks[0].border.minX, _controller.chunks[_last].border.maxX);
    }

    private void RightChunkEnable(int index)
    {
        //Right enable
        
        if (index < 1)
            return;

        var moveChunk = _controller.chunks[0];
        var newPosition = _controller.chunks[_last].transform.position;
        newPosition.x += moveChunk.width;
        moveChunk.transform.position = newPosition;


        for (int i = 0; i < _count - 1; i++)
        {
            _controller.chunks[i] = _controller.chunks[i + 1];
            _controller.chunks[i].index = i;
            _controller.chunks[i].last = false;
        }

        moveChunk.index = _count - 1;
        _controller.chunks[_last] = moveChunk;
        _controller.chunks[_last].last = true;
        _controller.border.AssignBorder(_controller.chunks[0].border.minX, _controller.chunks[_last].border.maxX);
    }

    public void RemoveStartChunk()
    {
        LeftChunkEnable(0);
    }

    public LevelChunk GetChunk(int index)
    {
        return _controller.chunks[index];
    }
    
    private void OnDestroy()
    {
        for (int i = 0; i < _count; i++)
        {
            _controller.chunks[i].OnChunkEnter.RemoveListener(LeftChunkEnable);
            _controller.chunks[i].OnChunkExit.RemoveListener(RightChunkEnable);
        }
    }
}
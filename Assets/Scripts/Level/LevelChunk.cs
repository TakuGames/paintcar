﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;



[Serializable]
public class IndexChunkEvent : UnityEvent<int> { }

[RequireComponent(typeof(BoxCollider2D))]
[ExecuteInEditMode]
public class LevelChunk : AChunk
{
    public ECity city;
    
    public int index;
    public bool last;
    
    public IndexChunkEvent OnChunkEnter = new IndexChunkEvent();
    public IndexChunkEvent OnChunkExit = new IndexChunkEvent();

    private void OnTriggerExit2D(Collider2D collision)
    {
        var tag = collision.tag;

        switch (tag)
        {
            case "Player":
                PlayerInTrigger(collision);
                break;
            case "Enemy":
                EnemyInTrigger(collision);
                break;
            default:
                return;
        }
    }

    private void EnemyInTrigger(Collider2D collision)
    {
        if (last)
            return;
        
        var centerX = transform.position.x + _trigger.offset.x;
        if (collision.transform.position.x > centerX)
        {
            collision.GetComponent<ANPCController>().PlaceInChunk(_chunkSwitcher.GetChunk(index + 1));
        }
        else if (collision.transform.position.x < centerX)
        {
            collision.GetComponent<ANPCController>().PlaceInChunk(this);
        }
    }

    private void PlayerInTrigger(Collider2D collision)
    {
        var centerX = transform.position.x + _trigger.offset.x; //центр триггера в мировых координатах
        if (collision.transform.position.x > centerX)
        {
            OnChunkExit.Invoke(index);
        }
        else if (collision.transform.position.x < centerX)
        {
            OnChunkEnter.Invoke(index);
        }
    }


}

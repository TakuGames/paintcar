﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public abstract class AChunk : MonoBehaviour
{
    [SerializeField] private Tilemap _map;
    protected BoxCollider2D _trigger;
    protected ChunkSwitcher _chunkSwitcher;
    private Border _border;

    public Border border
    {
        get
        {
//            _border.maxY = transform.position.y + trigger.size.y * 0.5f - offset.y - map.size.y * 0.5f;
//            _border.minY = transform.position.y - trigger.size.y * 0.5f - offset.y - map.size.y * 0.5f;
            _border.maxY = trigger.bounds.max.y;
            _border.minY = trigger.bounds.min.y;
            _border.maxX = transform.position.x + map.size.x * 0.5f + offset.x;
            _border.minX = transform.position.x - map.size.x * 0.5f + offset.x;
            
            return _border;
        }
    }
    
    public int width => map.size.x; //ширина
    public Vector2 offset => map.cellBounds.center; //смещение центра

    public Tilemap map => _map;
    
    private BoxCollider2D trigger
    {
        get
        {
            if (_trigger == null)
                _trigger = GetComponent<BoxCollider2D>();

            return _trigger;
        }
    }
    
    protected void Awake()
    {
        _chunkSwitcher = GetComponentInParent<ChunkSwitcher>();
        trigger.isTrigger = true;
    }
    
    //устанавливать размер и смещение
    private void SetTriggerSizeAndOffset()
    {
        _map.CompressBounds();
        var xOffset = _map.size.x * 0.5f + _map.cellBounds.center.x - 0.5f;
        var yOffset = _map.cellBounds.center.y;
        _trigger.offset = new Vector2(xOffset, yOffset);
        _trigger.size = new Vector2(1, _map.size.y);
    }
    
    [ContextMenu("Reset trigger")]
    public void ResetTrigger()
    {
        SetTriggerSizeAndOffset();
    }
    
}

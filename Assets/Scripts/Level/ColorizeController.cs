﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorizeController : MonoBehaviour
{
    [SerializeField] private float _speedColorizing = 0.5f;
    private List<ColorizeBackground> _colorizeElements;

    private IEnumerator Discoloring()
    {
        WaitForEndOfFrame endFrame = new WaitForEndOfFrame();

        float lerpSaturation = 1;

        for (int i = 0; i < _colorizeElements.Count; i++)
        {
            _colorizeElements[i].Colorization(false);
        }

        while (lerpSaturation > 0)
        {
            lerpSaturation -= _speedColorizing * Time.deltaTime;
            lerpSaturation = Mathf.Clamp01(lerpSaturation);

            float reverseSaturation = 1 - lerpSaturation;

            yield return endFrame;

            for (int i = 0; i < _colorizeElements.Count; i++)
            {
                if (_colorizeElements[i].isActiveAndEnabled)
                    _colorizeElements[i].ColoringToMono(lerpSaturation, reverseSaturation);
            }
        }

        for (int i = 0; i < _colorizeElements.Count; i++)
        {
            _colorizeElements[i].isColor = true;
        }
        
        StateGame.OnPlayGame.RemoveListener(() => { StartCoroutine(Discoloring()); });
    }

    private void Awake()
    {
        StateGame.OnPlayGame.AddListener(() => { StartCoroutine(Discoloring()); });
    }

    private void Start()
    {
        _colorizeElements = new List<ColorizeBackground>(FindObjectsOfType<ColorizeBackground>());

        foreach (var chunk in Reference.instance.levelController.chunks)
        {
            var items = chunk.GetComponentsInChildren<ColorizeBackground>(true);
            foreach (var item in items)
            {
                _colorizeElements.Add(item);
            }
        }

        for (int i = 0; i < _colorizeElements.Count; i++)
        {
            _colorizeElements[i].Colorization(true);
        }
    }
    private void Update()
    {
        for (int i = 0; i < _colorizeElements.Count; i++)
        {
            _colorizeElements[i].ColoringBackground();
        }
    }
}

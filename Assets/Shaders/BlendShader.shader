﻿Shader "Unlit/BlendShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_SecondaryTex("2ds Texture", 2D) = "white" {}
		_LerpValue("Transition float" , Range(0,1)) = 0.5
	}

		SubShader
		{
			Tags
		{
			"IgnoreProjector" = "True"
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

			ZWrite Off
			Lighting Off
			Cull Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#pragma multi_compile _ PIXELSNAP_ON
				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float4 color    : COLOR;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					fixed4 color : COLOR;
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D _SecondaryTex;
				float4 _SecondaryTex_ST;

				float _LerpValue;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = lerp(tex2D(_MainTex, i.uv) , tex2D(_SecondaryTex ,i.uv), 1 - _LerpValue);
					return col;
				}
				ENDCG
			}
		}
}

﻿Shader "Custom/SpriteFilling"
{
	Properties
	{
		_MainTex("Sprite Texture", 2D) = "white" {}
		_SecTex("Sprite Texture", 2D) = "white" {}

		_HorizontalFilling("Horizontal Filling", Range(0, 1)) = 1.0
		[Toggle] _HorizontalDirection("Horizontal Direction", float) = 1
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoordMain : TEXCOORD0;
                	float2 texcoordSec : TEXCOORD1;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoordMain : TEXCOORD0;
                	float2 texcoordSec : TEXCOORD1;
				};

				half _HorizontalFilling;
				float _HorizontalDirection;

				v2f vert(appdata_t IN)
				{
				  v2f OUT;
                  OUT.vertex = UnityObjectToClipPos(IN.vertex);
                  OUT.texcoordMain = IN.texcoordMain;
                  OUT.texcoordSec = IN.texcoordSec;
                  OUT.color = IN.color;
                  return OUT;
				}

				sampler2D _MainTex;
				sampler2D _SecTex;

				fixed4 SampleSpriteTexture(sampler2D tex,float2 uv)
				{
					fixed4 color = tex2D(tex, uv);
					return color;
				}

				fixed4 frag(v2f IN) : SV_Target
				{
					fixed4 main = SampleSpriteTexture(_MainTex , IN.texcoordMain);
					fixed4 sec = SampleSpriteTexture(_SecTex , IN.texcoordSec);

					if ((_HorizontalDirection == 1 && IN.texcoordMain.x > _HorizontalFilling) ||
						(_HorizontalDirection == 0 && IN.texcoordMain.x < (1 - _HorizontalFilling)))
						{
							main.a = 0;
						}
					else{
						sec.a = 0;
					}
					main.rgb *= main.a;
					sec.rgb *= sec.a;	

					return main + sec;
				}
			ENDCG
			}
		}
}

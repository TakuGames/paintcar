﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/GreyScale" {
	Properties{

		_MainTex("Texture", 2D) = "white" { }
		_Offset("Gray Color", Vector) = (0.3, 0.59, 0.11)
		_GrayValue("Color Multiplier", Range(0, 1)) = 0
		_BlueColor("Sepia Color", Range(0, 1)) = 0
	}
		SubShader{

			Tags {
					 "IgnoreProjector" = "True"
					 "Queue" = "Transparent"
					 "RenderType" = "Transparent"
					 }

			ZWrite Off
			Lighting Off
			Cull Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

			Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;

			float3 _Offset;

			float _GrayValue;
			float _BlueColor;

			half4 _currentColor;
			half4 _grayColor;

			struct v2f {
				float4  pos : SV_POSITION;
				float2  uv : TEXCOORD0;
			};

			float4 _MainTex_ST;

			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				half4 texcol = tex2D(_MainTex, i.uv);

				_currentColor = tex2D(_MainTex, i.uv);
				_grayColor = dot(texcol, _Offset);

				_grayColor.b -= _BlueColor;

				fixed3 col = lerp(_currentColor, _grayColor, 1 - _GrayValue);
				texcol.rgb = col;
				return texcol;
			}
			ENDCG
			}
		}
			Fallback "VertexLit"
}
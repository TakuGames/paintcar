﻿Shader "Unlit/VeniaminShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}

		_Intensity("Intensity", Vector) = (0.299, 0.587, 0.114,0)
		_Brightness("Brightness", Range(-1, 1)) = 0.
		_Contrast("Contrast", Range(0, 1)) = 1
		_RedColor("Red Color", Range(0, 1)) = 0.025
		_GreenColor("Green Color", Range(0, 1)) = 0.025
		_Saturation("Saturation", Range(0, 1)) = 1

		[Toggle] _IsShowColor("IsShowColor", int) = 0
	}
		SubShader
		{
			Tags
			{
				"IgnoreProjector" = "True"
				"Queue" = "Transparent"
				"RenderType" = "Transparent"
			}

			ZWrite Off
			Lighting Off
			Cull Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			int _IsShowColor;

			float _Brightness;
			float _Contrast;

			float _Saturation;

			float _RedColor;
			float _GreenColor;

			float3 _Intensity;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			inline float4 applyHSBEffect(float4 startColor)
			{
				float4 outputColor = startColor;
				outputColor.rgb = (outputColor.rgb - 0.5f) * (_Contrast)+0.5f;
				outputColor.rgb = outputColor.rgb + _Brightness;

				float3 intensity = dot(outputColor.rgb, _Intensity);

				outputColor.rgb = lerp(intensity, outputColor.rgb, _Saturation);

				outputColor.r += _RedColor;
				outputColor.g += _GreenColor;	
				return outputColor;
			}
			fixed4 frag(v2f i) : SV_Target
			{			
				if (_IsShowColor == 1)
				{
					_Brightness = 0;
					_Contrast = 1;
					_RedColor = 0;
					_GreenColor = 0;
					_Saturation = 1;
				}
				float4 startColor = tex2D(_MainTex, i.uv);
				float4 hsbColor = applyHSBEffect(startColor);
				return hsbColor;
			}
			ENDCG
		}
	}
	Fallback "VertexLit"
}
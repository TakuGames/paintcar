﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Animations;

public class SMBExit : StateMachineBehaviour
{
    [SerializeField] private bool _isExitWithReload = false;
    private int _firstTime = 0;

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _firstTime++;

        if (_firstTime < 2)
            return;

        Time.timeScale = 1;

        if (_isExitWithReload)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

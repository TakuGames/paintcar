﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ObjectPropertiesWindow : EditorWindow
{
    [MenuItem("Window/Object Properties")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ObjectPropertiesWindow), true, "Object");
    }

    private GameObject _go;
    private GUIStyle _bold = new GUIStyle();

    private void OnGUI()
    {
        DesignerEditor.objectWin = this;
        
        if (_go == null)
            return;

        Name();

        if (!ShowContent())
            return;

        Tag();
        Flip();
        Place();
        Delete();
    }

    private void OnDestroy()
    {
        DesignerEditor.objectWin = null;
    }

    private bool ShowContent()
    {
        if (_go.GetComponent<Grid>() != null)
            return false;

        if (_go.GetComponent<LevelChunk>() != null)
            return false;

        if (_go.GetComponent<Tilemap>() != null)
            return false;

        return true;
    }

    private void Name()
    {
        _bold.fontStyle = FontStyle.Bold;
        GUILayout.Space(20f);
        GUILayout.Label($"  {_go.name}", _bold);
        GUILayout.Space(20f);
    }

    private void Tag()
    {
        BeginBlock("tag to participate in the activation of stars", false);

        var tagComponent = _go.GetComponent<TaggedObject>();
        if (tagComponent != null)
        {
            var oldTag = tagComponent.tag;
            tagComponent.tag = (ETag) EditorGUILayout.EnumPopup("Tag", tagComponent.tag);

            if (oldTag != tagComponent.tag)
            {
                var sr = _go.GetComponent<SpriteRenderer>();
                switch (tagComponent.tag)
                {
                    case ETag.ZeroStars:
                        sr.sprite = (Sprite) AssetDatabase.LoadAssetAtPath("Assets/Designer/star0.png", typeof(Sprite));
                        break;
                    case ETag.OneStar:
                        sr.sprite = (Sprite) AssetDatabase.LoadAssetAtPath("Assets/Designer/star1.png", typeof(Sprite));
                        break;
                    case ETag.TwoStars:
                        sr.sprite = (Sprite) AssetDatabase.LoadAssetAtPath("Assets/Designer/star2.png", typeof(Sprite));
                        break;
                }
            }
        }

        EditorGUILayout.BeginHorizontal();
        if (tagComponent != null)
        {
            GUILayout.Label("Remove Tag");
            if (GUILayout.Button("Remove", GUILayout.Width(100), GUILayout.Height(20)))
            {
                DestroyImmediate(tagComponent);
                DestroyImmediate(_go.GetComponent<SpriteRenderer>());
            }
        }
        else
        {
            GUILayout.Label("Add Tag");
            if (GUILayout.Button("Add", GUILayout.Width(100), GUILayout.Height(20)))
            {
                _go.AddComponent<TaggedObject>();
                var sr = _go.AddComponent<SpriteRenderer>();
                sr.sprite = (Sprite) AssetDatabase.LoadAssetAtPath("Assets/Scripts/Designer/star0.png", typeof(Sprite));
                sr.sortingLayerName = "Foreground";
            }
        }

        EditorGUILayout.EndHorizontal();

        EndBlock(false);
    }

    private void Flip()
    {
        BeginBlock("Flip Object");

        if (GUILayout.Button("Flip", GUILayout.Width(100), GUILayout.Height(20)))
        {
            var spriteRenderers = _go.GetComponentsInChildren<SpriteRenderer>(true);
            foreach (var sr in spriteRenderers)
            {
                sr.flipX = !sr.flipX;
            }
        }

        EndBlock();
    }

    private void Place()
    {
        BeginBlock("Place Object On Ground");

        if (GUILayout.Button("Place", GUILayout.Width(100), GUILayout.Height(20)))
        {
            ObjectPlacer.Place(_go.GetComponent<Collider2D>());
        }

        EndBlock();
    }

    private void Delete()
    {
        BeginBlock("Delete Object");

        if (GUILayout.Button("Delete", GUILayout.Width(100), GUILayout.Height(20)))
        {
            DestroyImmediate(_go);
        }

        EndBlock();
    }

    private void OnInspectorUpdate()
    {
        var objects = Selection.gameObjects;
        _go = objects.Length > 1 || objects.Length <= 0 ? null : objects[0];

        Repaint();
    }

    private void BeginBlock(string label, bool horizontal = true)
    {
        EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(400));
        GUILayout.Space(10f);

        if (horizontal)
        {
            EditorGUILayout.BeginHorizontal();
        }

        GUILayout.Label(label);
    }

    private void EndBlock(bool horizontal = true)
    {
        if (horizontal)
        {
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.Space(10f);
        EditorGUILayout.EndVertical();
    }
}
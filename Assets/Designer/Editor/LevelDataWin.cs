﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class LevelDataWin : EditorWindow
{
    private Vector2 _scroll;
    private Editor _levelDataEditor;
    
    private void OnGUI()
    {
        DesignerEditor.levelDataWin = this;
        var data = (SOLevelsData) AssetDatabase.LoadAssetAtPath($"Assets/Levels/LevelsData.asset", typeof(SOLevelsData));
        _levelDataEditor = Editor.CreateEditor(data);

        _scroll = EditorGUILayout.BeginScrollView(_scroll);
        _levelDataEditor.OnInspectorGUI();
        EditorGUILayout.EndScrollView();
    }

    private void OnDestroy()
    {
        DesignerEditor.levelDataWin = null;
    }
}


//_placeObjects[i].scrollValue = GUILayout.BeginScrollView(_placeObjects[i].scrollValue, DStyle.layoutGroup(_placeObjects.Count, _winWidth, _winHeight));
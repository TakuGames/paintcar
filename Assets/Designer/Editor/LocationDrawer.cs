﻿using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;


[CustomPropertyDrawer(typeof(SOLevelsData.Location))]
public class LocationDrawer : PropertyDrawer
{
    private bool _groupEnabled = false;
    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var prof = property.FindPropertyRelative("_prof");
        
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.PrefixLabel(position, label);

        
        property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, label, DesignerEditor.DStyle.foldout);
        
        if (property.isExpanded)
        {
            EditorGUILayout.PropertyField(property.FindPropertyRelative("name"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("available"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("starsForOpen"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("levels"), true);

            prof.boolValue = EditorGUILayout.ToggleLeft("Pro Setup", prof.boolValue);
            
            if (prof.boolValue)
            {
                EditorGUILayout.PropertyField(property.FindPropertyRelative("backgroundPrefab"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("arrivalChunkPrefab"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("startChunkPrefab"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("backgroundLoadLevel"));
            }
        }
        EditorGUILayout.EndVertical();
    }
    
//    public override VisualElement CreatePropertyGUI(SerializedProperty property)
//    {
//        Debug.Log("OK");
//        var container = new VisualElement();
//        var imgui = new IMGUIContainer();
//        var name = new PropertyField(property.FindPropertyRelative("name"), "CustomLabel: Name");
//        var available = new PropertyField(property.FindPropertyRelative("available"));
//        container.Add(imgui);
//        container.Add(name);
//        container.Add(available);
//
//        return container;
//    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NewLevelWin : EditorWindow
{
    private ECity _city;
    private int _chunksCount = 3;

    public static void ShowWindow()
    {
        GetWindow(typeof(NewLevelWin), true, "New Level");
    }

    private void OnGUI()
    {
        _city = (ECity) EditorGUILayout.EnumPopup("Select City", _city);
        _chunksCount = EditorGUILayout.IntSlider("Chunks Count", _chunksCount, 3, 10);

        if (GUILayout.Button("Create"))
        {
            Create();
        }
    }

    private void Create()
    {
        var path = DesignerEditor.CreateFolder("Save New Level", $"Assets/Levels/{_city}", $"Level{_city}.asset");
        if (path == null)
            return;
        
        var levelData = ScriptableObject.CreateInstance<LevelData>();
        var locationData = DesignerEditor.designerData.GetLocationData(_city);
        levelData.city = _city;
        levelData.startChunk = locationData.startChunkMaster;
        levelData.levelChunks = new GameObject[_chunksCount];

        var instance = (GameObject) PrefabUtility.InstantiatePrefab(locationData.chunkMaster, DesignerEditor.root);
        
        for (int i = 0; i < _chunksCount; i++)
        {
            var prefabPath = path.Replace(".asset", $"Chunk_{i}.prefab");
            var prefab = PrefabUtility.SaveAsPrefabAsset(instance, prefabPath);
            levelData.levelChunks[i] = prefab;
        }
        
        DestroyImmediate(instance);

        path = path.Replace(Application.dataPath, "Assets");
        AssetDatabase.CreateAsset(levelData, path);
        
        DesignerEditor.PlaceLevel(levelData, path);
        
        DesignerEditor.newLevelWin.Close();
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class DesignerEditor : EditorWindow
{
    #region Styles

    public static class DStyle
    {
        public static GUILayoutOption[] layoutGroup(int value, float with, float height)
        {
            var layout = new GUILayoutOption[]
            {
                GUILayout.MaxWidth((with - 7) / value),
                GUILayout.MaxHeight(height)
            };

            return layout;
        }

        public static GUILayoutOption[] layout(float with, float height)
        {
            var layout = new GUILayoutOption[]
            {
                GUILayout.Width(with - 7),
                GUILayout.Height(height),
            };

            return layout;
        }

        public static GUIStyle foldout
        {
            get
            {
                var style = new GUIStyle();
                style = EditorStyles.foldout;
                style.fontStyle = FontStyle.Bold;
                style.alignment = TextAnchor.MiddleLeft;
                return style;
            }
        }

        public static GUIStyle bold
        {
            get
            {
                var style = new GUIStyle();
                style = EditorStyles.boldLabel;
                style.fontStyle = FontStyle.Bold;
                style.alignment = TextAnchor.MiddleCenter;
                return style;
            }
        }
    }

    #endregion

    private class ChunkData
    {
        public GameObject go;
        public LevelChunk chunk;
        public string path;
        public bool show;
        public Transform[] rootsObjects;
    }

    private class PlaceObjects
    {
        public string name;
        public List<DesignerElement> objects;
        public bool foldout;
        public Vector2 scrollValue;
    }

    private static LevelData _levelData;
    private static List<ChunkData> _chunkDatas;
    private static DesignerData _designerData;
    private static LocationData _locationData;
    private static Transform _root;
    private static bool _isOpenDesigner;
    private static bool _isReloaded;
    private static List<PlaceObjects> _placeObjects;

    private static ObjectPropertiesWindow _objectWin;
    private static NewLevelWin _newLevelWin;
    private static LevelDataWin _levelDataWin;
    private static EditorWindow _window;
    

    #region Properties

    private static float _winWidth => window.position.width;
    private static float _winHeight => window.position.height;


    public static bool isOpennDesigner => _isOpenDesigner;

    private static EditorWindow window
    {
        get
        {
            if (_window == null)
                _window = GetWindow(typeof(DesignerEditor));

            return _window;
        }
    }

    public static DesignerData designerData
    {
        get
        {
            if (_designerData == null)
                _designerData =
                    (DesignerData) AssetDatabase.LoadAssetAtPath("Assets/Designer/DesignerData.asset",
                        typeof(DesignerData));
            return _designerData;
        }
    }

    public static Transform root
    {
        get
        {
            if (_root == null)
                _root = FindObjectOfType<Grid>().transform;
            return _root;
        }

        private set => _root = value;
    }

    public static NewLevelWin newLevelWin
    {
        get
        {
            if (_newLevelWin == null)
                _newLevelWin = GetWindow<NewLevelWin>();

            return _newLevelWin;
        }
    }

    public static LevelDataWin levelDataWin
    {
        set => _levelDataWin = value;
    }

    public static ObjectPropertiesWindow objectWin
    {
        set => _objectWin = value;
    }

    #endregion

    [MenuItem("Window/Designer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(DesignerEditor));
    }

    private void OnInspectorUpdate()
    {
        if(Selection.gameObjects.Length <= 0)
            return;
        
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            var collider = Selection.gameObjects[i].GetComponent<DesignerElement>();
            if (collider != null)
            {
                continue;
            }

            var colliderParent = Selection.gameObjects[i].GetComponentInParent<DesignerElement>();
            Selection.activeObject = colliderParent != null ? colliderParent.gameObject : null;
        }
    }

    private void OnGUI()
    {
        //PlayTestBtn();

        if (EditorApplication.isPlaying)
            return;

        StartDesigner();
        CloseDesigner();

        if (!_isOpenDesigner)
            return;

        LevelDataBtn();

        EditorGUILayout.BeginHorizontal();
        NewLevelBtn();
        OpenLevelDataBtn();
        SaveLevelDataBtn();
        SaveAsLevelDataBtn();
        ClearLevelDataBtn();
        DeleteLevelDataBtn();
        EditorGUILayout.EndHorizontal();

        ObjectPropertiesBtn();
        ChunksEnable();
        ObjectsButtons();
    }

    private void PlayTestBtn()
    {
        if (EditorApplication.isPlaying)
        {
            if (GUILayout.Button("Stop Play", DStyle.layout(_winWidth, 50)))
            {
                EditorApplication.ExitPlaymode();
                _isReloaded = false;
            }
        }
        else
        {
            if (!_isReloaded)
            {
                var scene = SceneManager.GetActiveScene();
                if (scene.name != "Designer")
                {
                    return;
                }
                else
                {
                    var path = PlayerPrefs.GetString("Path");
                    Open(path);
                    _isReloaded = true;
                }
            }
            
            if (GUILayout.Button("Start Play", DStyle.layout(_winWidth, 50)))
            {
                Save();
                EditorApplication.EnterPlaymode();
            }
        }
    }

    #region Header

    private void StartDesigner()
    {
        if (_isOpenDesigner)
            return;

        if (GUILayout.Button("Start Designer", DStyle.layout(_winWidth, 50)))
        {
            var path = AssetDatabase.GetAssetPath(designerData.designerScene);
            EditorSceneManager.OpenScene(path);

            if (_chunkDatas != null)
                Clear();

            _isOpenDesigner = true;
            _isReloaded = true;
        }
    }

    private void CloseDesigner()
    {
        if (!_isOpenDesigner)
            return;

        if (GUILayout.Button("Close Designer", DStyle.layout(_winWidth, 20)))
        {
            var path = AssetDatabase.GetAssetPath(designerData.gameScene);
            EditorSceneManager.OpenScene(path);
            _isOpenDesigner = false;
        }
    }

    #endregion

    #region Menu

    private void NewLevelBtn()
    {
        if (GUILayout.Button("New Level"))
        {
            newLevelWin.Show();
        }
    }

    private void OpenLevelDataBtn()
    {
        if (GUILayout.Button("Open Level"))
        {
            var path = EditorUtility.OpenFilePanel("Open", "Assets/Levels", "asset");
            if (path.Length == 0)
                return;

            path = path.Replace(Application.dataPath, "Assets");
            Open(path);
        }
    }

    private void Open(string path)
    {
        var levelData = (LevelData) AssetDatabase.LoadAssetAtPath(path, typeof(LevelData));
        PlaceLevel(levelData, path);
        _isOpenDesigner = true;
    }

    private void SaveLevelDataBtn()
    {
        if (_chunkDatas == null)
            return;

        if (GUILayout.Button("Save Level"))
        {
            Save();
        }
    }

    public static string CreateFolder(string title, string directory, string defaultName, string extension = "asset")
    {
        var path = EditorUtility.SaveFilePanel(title, directory, defaultName, extension);
        if (path.Length == 0)
            return null;

        var i = path.LastIndexOf("/", StringComparison.Ordinal);
        var nameAsset = path.Substring(i + 1);
        var pathFolder = path.Replace(".asset", "");
        var pathAsset = $"{pathFolder}/{nameAsset}";

        Directory.CreateDirectory(pathFolder);

        return pathAsset;
    }

    private void SaveAsLevelDataBtn()
    {
        if (root.childCount <= 0)
            return;

        if (GUILayout.Button("Save As Level"))
        {
            var path = CreateFolder("Save As Level", "Assets/Levels", "Level.asset");
            if (path == null)
                return;

            SortingObjects();

            var levelData = ScriptableObject.CreateInstance<LevelData>();
            var locationData =
                DesignerEditor.designerData.GetLocationData(root.GetChild(0).GetComponent<LevelChunk>().city);

            if (locationData == null)
                return;

            levelData.city = locationData.city;
            levelData.startChunk = locationData.startChunkMaster;
            levelData.levelChunks = new GameObject[root.childCount];

            for (int i = 0; i < root.childCount; i++)
            {
                var chunk = root.GetChild(i).GetComponent<LevelChunk>();
                chunk.map.CompressBounds();
                levelData.levelChunks[i] = chunk.gameObject;

                var prefabPath = path.Replace(".asset", $"Chunk_{i}.prefab");
                var prefab = PrefabUtility.SaveAsPrefabAsset(chunk.gameObject, prefabPath);
                levelData.levelChunks[i] = prefab;
            }

            path = path.Replace(Application.dataPath, "Assets");
            AssetDatabase.CreateAsset(levelData, path);
        }
    }

    private void ClearLevelDataBtn()
    {
        if (root.childCount <= 0)
            return;

        if (GUILayout.Button("Clear"))
        {
            Clear();
        }
    }

    private void DeleteLevelDataBtn()
    {
        if (GUILayout.Button("Delete Level"))
        {
            var path = EditorUtility.OpenFilePanel("Delete Level", "Assets/Levels", "asset");
            if (path.Length == 0)
                return;

            var directoryInfo = new FileInfo(path).Directory;
            if (directoryInfo == null)
                return;

            if (!EditorUtility.DisplayDialog("Delete Level?", $"Are you sure delete {directoryInfo.Name} level?",
                "Delete", "Cancel"))
                return;

            var dirPath = directoryInfo.FullName;
            Directory.Delete(dirPath, true);
        }
    }

    #endregion

    #region ChunksEnable

    private void ChunksEnable()
    {
        if (_chunkDatas == null)
            return;

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Chunks Enable/Disable", DStyle.bold);
        EditorGUILayout.BeginHorizontal();

        for (int i = 0; i < _chunkDatas.Count; i++)
        {
            _chunkDatas[i].show = EditorGUILayout.ToggleLeft(_chunkDatas[i].go.name, _chunkDatas[i].show, DStyle.layoutGroup(_chunkDatas.Count, _winWidth, 30));
            _chunkDatas[i].go.SetActive(_chunkDatas[i].show);
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    #endregion

    #region PropertiesBtn

    private void ObjectPropertiesBtn()
    {
        if (root.childCount <= 0)
            return;

        if (_objectWin == null)
        {
            if (GUILayout.Button("Open Properties", DStyle.layout(_winWidth, 50)))
            {
                _objectWin = GetWindow<ObjectPropertiesWindow>();
                _objectWin.Show();
            }
        }
    }

    #endregion

    #region LevelDataBtn

    private void LevelDataBtn()
    {
        if (_levelDataWin == null)
        {
            if (GUILayout.Button("Level Data", DStyle.layout(_winWidth, 50)))
            {
                _levelDataWin = GetWindow<LevelDataWin>();
                _levelDataWin.Show();
            }
        }
    }

    #endregion

    #region Objects

    private static void ObjectsButtons()
    {
        if (_placeObjects == null)
            return;

        EditorGUILayout.BeginHorizontal();
        for (int i = 0; i < _placeObjects.Count; i++)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box,
                DStyle.layoutGroup(_placeObjects.Count, _winWidth, _placeObjects[i].foldout ? _winWidth : 20));
            _placeObjects[i].foldout =
                EditorGUILayout.Foldout(_placeObjects[i].foldout, _placeObjects[i].name, DStyle.foldout);
            if (_placeObjects[i].foldout)
            {
                _placeObjects[i].scrollValue = GUILayout.BeginScrollView(_placeObjects[i].scrollValue,
                    DStyle.layoutGroup(_placeObjects.Count, _winWidth, _winHeight));

                foreach (var obj in _placeObjects[i].objects)
                {
                    if (obj == null)
                    {
                        Debug.Log(obj.name);
                        continue;
                    }

                    if (GUILayout.Button(
                        obj.texture,
                        GUILayout.MaxHeight(Mathf.Clamp(obj.texture.height, 0, 100)),
                        GUILayout.MaxWidth(100)))
                    {
                        var go = (GameObject) PrefabUtility.InstantiatePrefab(obj.gameObject, root);
                        go.GetComponent<DesignerElement>().group = _placeObjects[i].name;
                    }
                }

                GUILayout.EndScrollView();
            }

            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndHorizontal();
    }

    #endregion

    #region Services

    private static void Save()
    {
        SortingObjects();

        foreach (var chunkData in _chunkDatas)
        {
            Debug.Log($"{chunkData.go.name}   {chunkData.path}");
            chunkData.go.SetActive(true);
            chunkData.go.GetComponent<LevelChunk>()?.map.CompressBounds();
            PrefabUtility.SaveAsPrefabAsset(chunkData.go, chunkData.path);
        }
    }

    private static void GetPlaceObjects()
    {
        DirectoryInfo dirResource = new DirectoryInfo($"Assets/Resources/{_levelData.city}");
        var dirGroup = dirResource.GetDirectories();
        _placeObjects = new List<PlaceObjects>();

        //Add all objects
        foreach (var dir in dirGroup)
        {
            PlaceObjects placeObjects = new PlaceObjects
                {name = dir.Name, objects = new List<DesignerElement>(), foldout = false};

            foreach (var item in dir.GetFiles())
            {
                if (item.Extension == ".prefab")
                {
                    var obj = (GameObject) AssetDatabase.LoadAssetAtPath($"{dirResource}/{dir.Name}/{item.Name}",
                        typeof(GameObject));
                    placeObjects.objects.Add(obj.GetComponent<DesignerElement>());
                }
            }

            _placeObjects.Add(placeObjects);
        }

        //Create Folders to Objects
        foreach (var chunk in _chunkDatas)
        {
            chunk.rootsObjects = new Transform[dirGroup.Length];
            for (int i = 0; i < dirGroup.Length; i++)
            {
                foreach (Transform child in chunk.go.transform)
                {
                    if (child.name == dirGroup[i].Name)
                    {
                        chunk.rootsObjects[i] = child;
                        break;
                    }
                }

                if (chunk.rootsObjects[i] == null)
                {
                    GameObject go = new GameObject(dirGroup[i].Name);
                    go.transform.parent = chunk.go.transform;
                    chunk.rootsObjects[i] = go.transform;
                }
            }
        }

        #region Testing

//            foreach (var test in _placeObjects)
//            {
//                foreach (var prefab in test.objects)
//                {
//                    var go = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
//                    go.GetComponent<DesignerElement>().group = test.name;
//                }
//            }

        #endregion
    }

    public static void PlaceLevel(LevelData levelData, string pathAsset)
    {
        Clear();

        PlayerPrefs.SetString("Path", pathAsset);

        _levelData = levelData;
        
        _locationData = designerData.GetLocationData(_levelData.city);
        _chunkDatas = new List<ChunkData>();

        foreach (var chunkPrefab in _levelData.levelChunks)
        {
            var chunkData = new ChunkData
            {
                go = chunkPrefab,
                path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(chunkPrefab)
            };
            _chunkDatas.Add(chunkData);
        }

        Vector3 position = Vector3.zero;
        foreach (var data in _chunkDatas)
        {
            var instance = (GameObject) PrefabUtility.InstantiatePrefab(data.go.gameObject, root);
            instance.transform.position = position;
            PrefabUtility.UnpackPrefabInstance(instance, PrefabUnpackMode.OutermostRoot, InteractionMode.UserAction);

            data.go = instance;
            data.show = true;
            data.chunk = instance.GetComponent<LevelChunk>();
            position.x += data.chunk.width;
        }

        GetPlaceObjects();
    }

    private static void SortingObjects()
    {
        var designerElements = FindObjectsOfType<DesignerElement>();
        if (designerElements.Length <= 0)
            return;

        Vector2[] boardChunks = new Vector2[_chunkDatas.Count];

        for (int i = 0; i < boardChunks.Length; i++)
        {
            boardChunks[i] = new Vector2(_chunkDatas[i].chunk.border.minX, _chunkDatas[i].chunk.border.maxX);
        }

        foreach (var de in designerElements)
        {
            float x = de.transform.position.x;

            for (int i = 0; i < boardChunks.Length; i++)
            {
                if (boardChunks[i].x <= x && x <= boardChunks[i].y)
                {
                    //write name group
                    if (String.IsNullOrEmpty(de.group))
                    {
                        foreach (var group in _placeObjects)
                        {
                            foreach (var obj in group.objects)
                            {
                                if (obj.texture == de.texture)
                                {
                                    de.group = group.name;
                                    break;
                                }
                            }
                        }
                    }

                    //placed objects in parents
                    foreach (var root in _chunkDatas[i].rootsObjects)
                    {
                        if (root.name == de.group)
                        {
                            de.transform.parent = root;
                            break;
                        }
                    }

                    break;
                }
            }
        }
    }

    private static void Clear()
    {
        _chunkDatas?.Clear();

        _locationData = null;
        _levelData = null;
        _placeObjects = null;

        var scene = SceneManager.GetActiveScene();
        var objects = scene.GetRootGameObjects();

        for (int i = 0; i < objects.Length; i++)
        {
            DestroyImmediate(objects[i]);
        }

        root = Instantiate(designerData.grid).transform;
        root.position = Vector3.zero;
    }

    #endregion
}
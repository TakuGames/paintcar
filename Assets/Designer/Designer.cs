﻿
using System;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine;





#if UNITY_EDITOR

public class Designer : MonoBehaviour
{
    private static Designer _instance;

    public static Designer instance
    {
        get
        {
            if (_instance == null)
                _instance = (Designer) FindObjectOfType(typeof(Designer));

            return _instance;
        }
    }

    [Header("Reference")]
    [SerializeField] private Transform _paintableRoot;
    [SerializeField] private Transform _spawnersRoot;
    [SerializeField] private Transform _objectsRoot;
    
    [Header("Locations Setup")]
    [SerializeField] private LocationData[] _locationsData;
    private GameObject _instanceRoot;

    public Transform paintableRoot
    {
        get
        {
            if (_paintableRoot == null)
                _paintableRoot = GameObject.Find("Paintable").transform;

            return _paintableRoot;
        }
    }
    
    public Transform spawnersRoot
    {
        get
        {
            if (_spawnersRoot == null)
                _spawnersRoot = GameObject.Find("Spawners").transform;

            return _spawnersRoot;
        }
    }
    
    public Transform objectsRoot
    {
        get
        {
            if (_objectsRoot == null)
                _objectsRoot = GameObject.Find("Objects").transform;

            return _objectsRoot;
        }
    }

    public void Clear()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        var scene = EditorSceneManager.GetActiveScene();
        var objects = scene.GetRootGameObjects();

        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].GetComponent<Grid>() == null)
                DestroyImmediate(objects[i]);
        }
    }
}

#endif
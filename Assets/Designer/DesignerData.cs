﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

[Serializable]
public class LocationData
{
    public ECity city;
    public GameObject startChunkMaster;
    public GameObject chunkMaster;
}

[CreateAssetMenu(fileName = "DesignerData", menuName = "DesignerData")]
public class DesignerData : ScriptableObject
{
    [SerializeField] private Object _designerScene;
    [SerializeField] private Object _gameScene;
    [SerializeField] private GameObject _grid;

    [SerializeField] private LocationData[] _locationsData;

    public Object designerScene => _designerScene;
    public Object gameScene => _gameScene;
    public GameObject grid => _grid;
    

    public LocationData GetLocationData(ECity city)
    {
        foreach (var location in _locationsData)
        {
            if (location.city == city)
            {
                return location;
            }
        }

        return null;
    }
}
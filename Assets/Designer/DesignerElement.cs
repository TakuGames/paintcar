﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesignerElement : MonoBehaviour
{
    public Texture texture;
    public string group;

    private void Awake()
    {
        DestroyImmediate(this);
    }
}

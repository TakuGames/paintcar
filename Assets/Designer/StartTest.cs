﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartTest : MonoBehaviour
{
    private void Start()
    {
        SceneManager.LoadSceneAsync("GameScene");
    }
}

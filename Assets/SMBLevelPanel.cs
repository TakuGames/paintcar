﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBLevelPanel : StateMachineBehaviour
{
    private Animator _animator;

    private void Awake()
    {
        UIPanelLevelBtn.OnClose.AddListener(SlideMenu);
    }
    private void OnDestroy()
    {
        UIPanelLevelBtn.OnClose.RemoveListener(SlideMenu);
    }

    void SlideMenu(bool swithcer)
    {
        _animator.SetBool("Open", !swithcer);
    }

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _animator = animator;
    }
}
